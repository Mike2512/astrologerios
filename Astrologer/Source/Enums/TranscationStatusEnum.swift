//
//  TranscationStatusEnum.swift
//  Astrologer
//
//  Created by George Rousou on 06/08/2021.
//

import Foundation

enum TranscationStatusEnum: Int
{
    case Received = 1
    case Pending = 2
}
