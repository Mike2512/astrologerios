//
//  CallTypeEnum.swift
//
//  Created by George Rousou on 06/18/2021.
//

import Foundation

enum CallTypeEnum: Int
{
    case Audio = 1
    case Video = 2
}
