//
//  AstrologerViewCell.swift
//  Rich Astrology
//
//  Created by George Rousou on 13/10/2020.
//

import UIKit

class AstrologerViewCell: UITableViewCell {

   
    @IBOutlet weak var astrologerImage: UIImageView!
    @IBOutlet weak var astrologerName: UILabel!
    @IBOutlet weak var astrologerSubject: UILabel!
    @IBOutlet weak var astrologerCost: UILabel!
    @IBOutlet weak var astrologerLanguage: UILabel!
    @IBOutlet weak var callButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        astrologerImage.layer.cornerRadius = astrologerImage.layer.bounds.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
