//
//  CategoriesCollectionViewCell.swift
//  Rich Astrology
//
//  Created by George Rousou on 20/05/2021.
//

import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var categoryName: UILabel!
}
