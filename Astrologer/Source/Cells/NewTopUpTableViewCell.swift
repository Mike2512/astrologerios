//
//  NewTopUpTableViewCell.swift
//  Rich Astrology
//
//  Created by George Rousou on 27/05/2021.
//

import UIKit

class NewTopUpTableViewCell: UITableViewCell {

    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var amountWidth: NSLayoutConstraint!
    @IBOutlet weak var paymentReferenceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
