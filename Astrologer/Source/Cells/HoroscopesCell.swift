//
//  HoroscopesCell.swift
//  Rich Astrology
//
//  Created by George Rousou on 07/05/2021.
//

import UIKit

class HoroscopesCell: UITableViewCell {

    @IBOutlet weak var horoscopeImage: UIImageView!
    @IBOutlet weak var horoscopeName: UILabel!
    @IBOutlet weak var horoscopeDate: UILabel!
    @IBOutlet weak var cellView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cellView.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
