//
//  TopUpTableViewCell.swift
//  Rich Astrology
//
//  Created by George Rousou on 19/10/2020.
//

import UIKit

class TopUpTableViewCell: UITableViewCell {

    @IBOutlet weak var balanceAfter: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var amountWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
