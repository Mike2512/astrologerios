//
//  PreLoginCollectionViewCell.swift
//  Rich Astrology
//
//  Created by George Rousou on 21/05/2021.
//

import UIKit

class PreLoginCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var preLoginImage: UIImageView!
    @IBOutlet weak var preLoginMessage: UILabel!
    @IBOutlet weak var messageWidth: NSLayoutConstraint!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
}
