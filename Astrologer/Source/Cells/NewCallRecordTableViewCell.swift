//
//  NewCallRecordTableViewCell.swift
//  Rich Astrology
//
//  Created by George Rousou on 21/05/2021.
//

import UIKit

class NewCallRecordTableViewCell: UITableViewCell {

    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var callTo: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var rate: UILabel!
    @IBOutlet weak var astrologerImage: UIImageView!
    @IBOutlet weak var totalMinLabel: UILabel!
    @IBOutlet weak var astrologerWidth: NSLayoutConstraint!
    @IBOutlet weak var offerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        astrologerImage.layer.cornerRadius = astrologerImage.layer.bounds.height / 2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
