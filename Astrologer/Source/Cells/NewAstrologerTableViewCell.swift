//
//  NewAstrologerTableViewCell.swift
//  Rich Astrology
//
//  Created by George Rousou on 10/05/2021.
//

import UIKit

class NewAstrologerTableViewCell: UITableViewCell {

    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var language: UILabel!
    @IBOutlet weak var astrologerPriceAfterOffer: UILabel!
    @IBOutlet weak var astrologerPrice: UILabel!
    @IBOutlet weak var astrologerDescription: UILabel!
    @IBOutlet weak var offerLabel: UILabel!
    @IBOutlet weak var astrologerName: UILabel!
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var astrologerImage: UIImageView!
    @IBOutlet weak var nameWidth: NSLayoutConstraint!
    
    
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        astrologerImage.layer.cornerRadius = astrologerImage.layer.bounds.height / 2
        cellView.layer.cornerRadius = 5
    }
    
    
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
