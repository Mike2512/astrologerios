//
//  CallRecordTableViewCell.swift
//  Rich Astrology
//
//  Created by George Rousou on 19/10/2020.
//

import UIKit

class CallRecordTableViewCell: UITableViewCell {

    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var callTo: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var charge: UILabel!
    @IBOutlet weak var newBalance: UILabel!
    @IBOutlet weak var chargeWidth: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
