//
//  TabsViewCell.swift
//  Rich Astrology
//
//  Created by George Rousou on 13/10/2020.
//

import UIKit

class TabsViewCell: UITableViewCell {

    @IBOutlet weak var tabImage: UIImageView!
    @IBOutlet weak var tabName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
