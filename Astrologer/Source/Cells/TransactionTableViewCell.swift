//
//  TransactionTableViewCell.swift
//  Astrologer
//
//  Created by George Rousou on 06/08/2021.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    @IBOutlet weak var transactionImage: UIImageView!
    @IBOutlet weak var transactionStatus: UILabel!
    @IBOutlet weak var transactionValue: UILabel!
    @IBOutlet weak var transactionDate: UILabel!
    @IBOutlet weak var transactionMadeBy: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
