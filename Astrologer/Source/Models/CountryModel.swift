//
//  CountryModel.swift
//  Rich Astrology
//
//  Created by George Rousou on 12/10/2020.
//

import Foundation
import UIKit

class CountryModel
{
    var countryID: Int!
    var countryName : String!
    var countryShortName : String!
    var countryFlag : UIImage!
    var isSelected : Bool! = false
}


