//
//  UserModel.swift
//  Rich Astrology
//
//  Created by George Rousou on 15/10/2020.
//

import Foundation

class UserModel : Codable
{
    var countryCode : Int!
    var userPhone : Int!
    var userName : String!
    var sinchUserID : String!
    var sinchUserPassword : String!
    var registrationDateTime : String!
    var stripeID : String!
    var balance : Double!
    var totalCallsMade : Int!
    var totalDurationSecs : Int!
    var rowID : Int!
    
    private enum CodingKeys : String, CodingKey {
        case countryCode = "CountryCode"
        case userPhone = "MobileNumber"
        case userName = "user_name"
        //case sinchUserID = "SinchUserID"
        //case sinchUserPassword = "SinchUserPassword"
        case registrationDateTime = "RegistrationDateTime"
        case balance = "Balance"
        case totalCallsMade = "TotalCallsMade"
        case totalDurationSecs = "TotalDurationSecs"
        case stripeID = "stripe_user_id"
        case rowID = "row_id"
    }
}
