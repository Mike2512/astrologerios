//
//  PreLoginImageModel.swift
//  Rich Astrology
//
//  Created by George Rousou on 21/05/2021.
//

import Foundation
import UIKit


class PreLoginImageModel
{
    var preImage : UIImage!
    var preName : String!
}
