//
//  TransactionModel.swift
//  Astrologer
//
//  Created by George Rousou on 06/08/2021.
//

import Foundation


class TransactionModel
{
    var transcationID: Int!
    var transcactionType : Int!
    var transcactionStatus : Int!
    var transcactionMadeBy : String!
    var transcactionDate : String!
    var transcactionValue : Double!
    var isSelected : Bool! = false
}
