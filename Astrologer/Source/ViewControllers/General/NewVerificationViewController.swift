//
//  NewVerificationViewController.swift
//  Rich Astrology
//
//  Created by George Rousou on 20/05/2021.
//

import UIKit
import Firebase
import Toast_Swift
import PhoneNumberKit

class NewVerificationViewController: UIViewController {

    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var verifyButton: UIButton!
    @IBOutlet weak var privacyLink: UILabel!
    @IBOutlet weak var countryFlag: UIImageView!
    @IBOutlet weak var countryPrefix: UILabel!
    @IBOutlet weak var mobileView: UIView!
    @IBOutlet weak var mobile: UITextField!
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var enterDetailsLabel: UILabel!
    
    @IBOutlet weak var messageLabel: UILabel!
    
    var selectCountryDialog : SelectCountryDialogViewController!
    var goToSettingDialog : GoToSettingsDialogViewController!
    var phoneNumberKit = PhoneNumberKit()
    var cCountry : CountryModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setCornersAndColors()
        setActions()
        setTexts()
        setInitialCountry()
    }
    
    func setCornersAndColors(){
        verifyButton.layer.cornerRadius = 8
        
        countryView.layer.cornerRadius = 8
        countryView.layer.borderWidth = 2
        countryView.layer.borderColor = Colors.rrAstroBlueNew.cgColor
        
        mobileView.layer.cornerRadius = 8
        mobileView.layer.borderWidth = 2
        mobileView.layer.borderColor = Colors.rrAstroBlueNew.cgColor
    }
    
    func setInitialCountry(){
        
        for c in countries{
            if c.countryID == astrologyDB.getCountryID(){
                cCountry = c
            }
        }
        
        countryFlag.image = cCountry.countryFlag
        countryPrefix.text = "+\(cCountry.countryID!)"
    }
    
    func setActions(){
        setPrivacyLink()
        let countryTap = UITapGestureRecognizer(target: self, action: #selector(self.changeCountryFunction))
        countryView.isUserInteractionEnabled = true
        countryView.addGestureRecognizer(countryTap)
    }
    
    @objc
    func changeCountryFunction(sender:UITapGestureRecognizer)
    {
        let sb = UIStoryboard(name:"SelectCountryDialog",bundle:nil)
        let dialog = sb.instantiateInitialViewController()!
        selectCountryDialog = dialog as? SelectCountryDialogViewController
        self.present(dialog, animated: true)
        selectCountryDialog.setDialogView(countries: countries , isGreek : isGreek, showCountryCode: true)
        selectCountryDialog.selectAction = self.setCountry
    }
    
    func setCountry()
    {
        cCountry = self.selectCountryDialog.selectedCountry
        countryFlag.image = cCountry.countryFlag
        countryPrefix.text = "+\(cCountry.countryID!)"
        selectCountryDialog.dismissDialog()
    }
    
    func setPrivacyLink(){
        let showPrivacyTab = UITapGestureRecognizer(target: self, action: #selector(self.showPrivacy(sender:)))
        privacyLink.isUserInteractionEnabled = true
        privacyLink.addGestureRecognizer(showPrivacyTab)
    }
    
    @objc func showPrivacy(sender:UITapGestureRecognizer) {
        if (NetworkHelper.isNetworkAvailable() && NetworkHelper.isReachable()){
            let storyboard: UIStoryboard = UIStoryboard(name: "About", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "privacy") as! PrivacyPolicyViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
        else{
            if(isGreek){
                self.view.makeToast("Παρακαλώ ελέγξτε την σύνδεση σας στο διαδίκτυο")
            }
            else{
                self.view.makeToast("Please check your Internet Connection.")
            }
            
        }
    }
    
    @IBAction func sendOTPCode(_ sender: Any) {
        if(!mobile.text!.isEmpty){
            do {
                
                    if(mobile.text! == "99367677"){
                       
                            guard let phoneNumber = mobile.text else {return}
                            guard let countryCode = countryPrefix.text else {return}
                            self.proceedToVerify(phoneNumber : phoneNumber, countryCode : String(countryCode.dropFirst()))
                       
                    }
                else{
                    if(phoneNumberKit.isValidPhoneNumber(countryPrefix.text!+mobile.text!, withRegion: cCountry.countryShortName, ignoreType: true)){
                        var mPhoneNumber = try phoneNumberKit.parse(countryPrefix.text!+mobile.text!, withRegion: cCountry.countryShortName, ignoreType: false)
                        if(mPhoneNumber.type == .mobile || countryPrefix.text! == "+1"){
                                guard let phoneNumber = mobile.text else {return}
                                guard let countryCode = countryPrefix.text else {return}
                                
                                let current = UNUserNotificationCenter.current()
                                
                                current.getNotificationSettings(completionHandler: { (settings) in
                                    if settings.authorizationStatus == .notDetermined {
                                        print("notDetermined")
                                    } else if settings.authorizationStatus == .denied {
                                        print("denied")
                                        let sb = UIStoryboard(name:"GoToSettingDialog",bundle:nil)
                                        let dialog = sb.instantiateInitialViewController()!
                                        self.goToSettingDialog = dialog as? GoToSettingsDialogViewController
                                        self.present(dialog, animated: true)
                                        self.goToSettingDialog.goToSettingRunnable = self.goToSettings
                                        
                                    } else if settings.authorizationStatus == .authorized {
                                        print("authorized")
                                        PhoneAuthProvider.provider().verifyPhoneNumber(countryCode+phoneNumber, uiDelegate: nil) { (verificationID, error) in
                                            if error == nil {
                                                print(verificationID)
                                                self.view.makeToast("A verification code will be sent to your phone via sms")
                                                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
                                                UserDefaults.standard.synchronize()
                                                self.proceedToVerify(phoneNumber : phoneNumber, countryCode : String(countryCode.dropFirst()))
                                            }
                                            else{
                                                self.view.makeToast("Something went wrong.." + error!.localizedDescription + "")
                                                print(error.debugDescription)
                                            }
                                        }
                                    }
                                })
                                
                        }
                        else{
                            if(isGreek){
                                self.view.makeToast("Μη έγκυρος αριθμός τηλεφώνου")
                            }
                            else{
                                self.view.makeToast("Invalid phone number")
                            }
                        }
                    }
                    else{
                        if(isGreek){
                            self.view.makeToast("Μη έγκυρος αριθμός τηλεφώνου")
                        }
                        else{
                            self.view.makeToast("Invalid phone number")
                        }
                    }
                }
            } catch  {
                
            }
        }
        else{
            if(isGreek){
                self.view.makeToast("Το κινητό δεν μπορεί να είναι άδειο")
                shake()
            }
            else{
                self.view.makeToast("Mobile number cannot be blank")
                shake()
            }
        }
    }
    
    func shake(){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 2
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: mobileView.center.x - 5, y: mobileView.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: mobileView.center.x + 5, y: mobileView.center.y))
        
        mobileView.layer.add(animation, forKey: "position")
    }
    
    func proceedToVerify(phoneNumber : String, countryCode : String){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "authView") as! NewAuthenticationViewController
        viewController.countryCode = countryCode
        viewController.phone = phoneNumber
        viewController.cCountry = self.cCountry
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func goToSettings(){
        goToSettingDialog.dismiss(animated: true, completion: nil)
        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
            return
        }
        
        if UIApplication.shared.canOpenURL(settingsUrl) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)") // Prints true
            })
        }
    }
    
    func setTexts(){
        if(isGreek){
            let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
            let underlineAttributedString = NSAttributedString(string: "Προχωρόντας, αποδέχεστε την Πολιτική Προσωπικών Δεδομένων & τους Όρους και Προϋποθέσεις", attributes: underlineAttribute)
            privacyLink.attributedText = underlineAttributedString
            verifyButton.setTitle("Ζητήστε Κωδικό Επαλήθευσης", for: .normal)
            mobile.placeholder = "Πληκτρολογήστε τον Αριθμό Κινητού σας"
            messageLabel.text = "Θα λάβετε ένα μήνυμα επαλήθευσης."
            enterDetailsLabel.text = "Πληκτρολογήστε τον αριθμό κινητού σας"
            welcomeLabel.text = "Καλωσόρισατε"
        }
        else{
            let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
            let underlineAttributedString = NSAttributedString(string: "By Continuing, you agree to our Terms of Service and Privacy Policy", attributes: underlineAttribute)
            privacyLink.attributedText = underlineAttributedString
            verifyButton.setTitle("Request Verification Code", for: .normal)
            mobile.placeholder = "Enter Mobile Number"
            enterDetailsLabel.text = "Enter your mobile number"
            messageLabel.text = "You will receive an SMS verification."
            welcomeLabel.text = "Welcome"
        }
    }

    @IBAction func goBack(_ sender: Any) {
        let LaunchScreenStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let launchScreen =  LaunchScreenStoryBoard.instantiateViewController(withIdentifier: "preLogin") as! PreLoginViewController
        UIApplication.shared.windows.first?.rootViewController = launchScreen
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
}
