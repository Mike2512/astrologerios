//
//  MyEarningViewController.swift
//  Astrologer
//
//  Created by George Rousou on 05/08/2021.
//

import UIKit

class MyEarningViewController: CallSetupViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var walletView: UIView!
    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var earningHistoryTable: UITableView!
    
    var recentTransactions : [TransactionModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        self.tabBarController?.tabBar.isHidden = true
        setNavigationTitle()
        walletView.layer.cornerRadius = 8
        walletView.layer.masksToBounds = true
        earningHistoryTable.delegate = self
        earningHistoryTable.dataSource = self
        earningHistoryTable.tableFooterView = UIView()
        
        setTransactions()
    }
    
    func setTransactions(){
        let t = TransactionModel ()
        t.transcactionMadeBy = "Michalis"
        t.transcactionDate = "16 Jun 21, 14:41"
        t.transcactionType = CallTypeEnum.Audio.rawValue
        t.transcactionStatus = TranscationStatusEnum.Received.rawValue
        t.transcactionValue = 13.50
        recentTransactions.append(t)
        let t2 = TransactionModel ()
        t2.transcactionMadeBy = "Panagiwta"
        t2.transcactionDate = "21 Jun 21, 14:41"
        t2.transcactionType = CallTypeEnum.Video.rawValue
        t2.transcactionStatus = TranscationStatusEnum.Pending.rawValue
        t2.transcactionValue = 21.80
        recentTransactions.append(t2)
        let t3 = TransactionModel ()
        t3.transcactionMadeBy = "Nikos"
        t3.transcactionDate = "20 Jun 21, 14:41"
        t3.transcactionType = CallTypeEnum.Audio.rawValue
        t3.transcactionStatus = TranscationStatusEnum.Received.rawValue
        t3.transcactionValue = 3.30
        recentTransactions.append(t3)
        let t4 = TransactionModel ()
        t4.transcactionMadeBy = "Andria"
        t4.transcactionDate = "17 Jun 21, 14:41"
        t4.transcactionType = CallTypeEnum.Video.rawValue
        t4.transcactionStatus = TranscationStatusEnum.Received.rawValue
        t4.transcactionValue = 11.92
        recentTransactions.append(t4)
        
        earningHistoryTable.reloadData()
        
    }
    
    func setNavigationTitle(){
        if(isGreek){
            navTitle.text = "Τα Κέρδη μου"
        }
        else{
            navTitle.text = "My Earnings"
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if recentTransactions.count > 0{
            return recentTransactions.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if recentTransactions.count > 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "transactionCell") as! TransactionTableViewCell
            
            let item = recentTransactions[indexPath.row]
            cell.transactionValue.text = "€\(item.transcactionValue!)"
            cell.transactionDate.text = item.transcactionDate ?? ""
            cell.transactionMadeBy.text = item.transcactionMadeBy ?? ""
            
            switch item.transcactionStatus {
            case TranscationStatusEnum.Pending.rawValue:
                cell.transactionImage.image = UIImage(named: "ic_pending_transaction")
                cell.transactionStatus.text = "Pending"
                cell.transactionStatus.textColor = Colors.rrAstroBlue
                break
            case TranscationStatusEnum.Received.rawValue:
                cell.transactionImage.image = UIImage(named: "ic_receivedTransaction")
                cell.transactionStatus.text = "Received"
                cell.transactionStatus.textColor = UIColor.green
                break
            default:
                break
            }
            cell.transactionValue.text = "€\(item.transcactionValue!)"
            cell.transactionValue.text = "€\(item.transcactionValue!)"
            
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = recentTransactions[indexPath.row]
        let tabBarStoryboard: UIStoryboard = UIStoryboard(name: "TabBarController", bundle: nil)
        let viewController = tabBarStoryboard.instantiateViewController(withIdentifier: "transactionDetails") as! TransactionDetailsViewController
        viewController.transactionDetail = item
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @IBAction func goBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
