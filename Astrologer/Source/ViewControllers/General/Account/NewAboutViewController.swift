//
//  NewAboutViewController.swift
//  Rich Astrology
//
//  Created by George Rousou on 18/05/2021.
//

import UIKit
import Toast_Swift
import Reachability

class NewAboutViewController: UIViewController {

    var reachability: Reachability!
    
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var privacyLink: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        setNavigationTitle()
        setTexts()
       
        setPrivacyLink()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setTexts(){
        if(isGreek){
            aboutLabel.text = "Σας ευχαριστούμε που χρησιμοποιείτε την εφαρμογή Astrologia RichReach Corporation (CY) Ltd\n\n\nΜπορείτε να επικοινωνήσετε μαζί μας:\n\n\u{2022} Με email:   info@richreachcorp.com\n\n\u{2022} Τηλεφωνικώς:+35722339999\n\n\u{2022} Ταχυδρομικώς:   Τ.Θ. 14179, 2154 Αγλαντζιά, Λευκωσία, Κύπρος\n\n\nΤα γραφεία μας βρίσκονται στη:\n\nΛεωφόρος Λάρνακος 61Α, Γραφείο 402, 2101 Αγλαντζιά, Λευκωσία, Κύπρος\n\n\nΑν είσαστε κάτοικος της Ευρωπαϊκής Ένωσης, οι τιμές περιλαμβάνουν 19% ΦΠΑ\nΑν είσαστε κάτοικος οποιασδήποτε άλλης χώρας, οι τιμές περιλαμβάνουν 0% ΦΠΑ\nΟ αριθμός μητρώου ΦΠΑ μας στην Κύπρο είναι 10329324Q\n\nΑφού πραγματοποιήσετε μια πληρωμή ή συνδεθείτε, περιμένετε λίγο για να ενημερωθεί το υπόλοιπό σας."
            
            let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
            let underlineAttributedString = NSAttributedString(string: "Πολιτική Προσωπικών Δεδομένων & τους Όρους και Προϋποθέσεις", attributes: underlineAttribute)
            privacyLink.attributedText = underlineAttributedString
            
        }
        else{
            aboutLabel.text = "\nThank you for using the Astrologia Application of RichReach Corporation (CY)Ltd\n\n\nYou can contact us:\n\n\u{2022} By email:   info@richreachcorp.com\n\n\u{2022} By telephone:   +35722339999\n\n\u{2022} By post:   PO Box 14179, 2154 Aglandjia, Nicosia, Cyprus\n\n\nOur offices are located at:\n\n61A Larnakos Avenue, Office 402, 2101 Aglandjia, Nicosia, Cyprus\n\n\nΙf you reside in an EU country, the prices include 19% VAT\nΙf you reside in a non-EU country, the prices include 0% VAT\nAfter registering or making a payment, please wait a bit for your balance to update."
            
            let underlineAttribute = [NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
            let underlineAttributedString = NSAttributedString(string: "Privacy Policy & Terms and Conditions", attributes: underlineAttribute)
            privacyLink.attributedText = underlineAttributedString
            
        }
    }
    
    func setPrivacyLink(){
        let showPrivacyTab = UITapGestureRecognizer(target: self, action: #selector(self.showPrivacy(sender:)))
        privacyLink.isUserInteractionEnabled = true
        privacyLink.addGestureRecognizer(showPrivacyTab)
    }
    
    
    @objc func showPrivacy(sender:UITapGestureRecognizer) {
        if (NetworkHelper.isNetworkAvailable() && NetworkHelper.isReachable()){
            let storyboard: UIStoryboard = UIStoryboard(name: "About", bundle: nil)
            let viewController = storyboard.instantiateViewController(withIdentifier: "privacy") as! PrivacyPolicyViewController
            self.navigationController!.pushViewController(viewController, animated: true)
        }
        else{
            if(isGreek){
                self.view.makeToast("Παρακαλώ ελέγξτε την σύνδεση σας στο διαδίκτυο")
            }
            else{
                self.view.makeToast("Please check your Internet Connection.")
            }
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setNavigationTitle(){
        if(isGreek){
            navTitle.text = "Σχετικά"
        }
        else{
            navTitle.text = "About"
        }
    }

}
