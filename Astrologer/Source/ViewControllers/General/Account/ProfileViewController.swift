//
//  ProfileViewController.swift
//  Astrologer
//
//  Created by George Rousou on 23/08/2021.
//

import UIKit
import DropDown

class ProfileViewController: UIViewController {

    @IBOutlet weak var navTitle: UILabel!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var mobileNumberLabel: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var internationalPreferencesLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var language: UILabel!
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var userNameWidth: NSLayoutConstraint!
    @IBOutlet weak var mobileWidth: NSLayoutConstraint!
    @IBOutlet weak var userNameView: UIView!
  
    
    let dropDown = DropDown()
    
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DropDown.appearance().textColor = UIColor.black
        DropDown.appearance().textFont = UIFont(name: "Poppins-Regular", size: 12.0)!
        DropDown.appearance().backgroundColor = UIColor.white
        
        userProfileImage.layer.cornerRadius = userProfileImage.layer.bounds.height / 2
        
        setActions()
        
        setView()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        setView()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func setActions(){
        let languageClick = UITapGestureRecognizer(target: self, action: #selector(self.languageClicked(sender:)))
        languageView.isUserInteractionEnabled = true
        languageView.addGestureRecognizer(languageClick)
    }
    
    @objc func languageClicked(sender:UITapGestureRecognizer) {
        dropDown.dataSource = ["English - Αγγλικά", "Greek - Ελληνικά"]
        dropDown.anchorView = languageView
        dropDown.bottomOffset = CGPoint(x: 0, y: languageView.frame.size.height)
        dropDown.show()
        dropDown.selectionAction = { [weak self] (index: Int, item: String) in
            guard let _ = self else { return }
            if(index == 0){
                isGreek = false
            }
            else{
                isGreek = true
            }
           
            astrologyDB.insertStartUpCountryAndLanguage(countryID: selectedCountry.countryID, isGreek: isGreek)
            if(astrologyDB.getLanguage() == "Greek"){
                isGreek = true
                self!.setGreekTabs()
            }
            else{
                isGreek = false
                self!.setEnglishTabs()
            }
            
            self!.viewDidLoad()
        }
        
       
    }
    
    func setView(){
        appUser = astrologyDB.getUser()
        if(appUser != nil){
        if(!isNullOrEmpty(string: appUser.userName)){
            userNameView.isHidden = false
          
            userName.text = "\(appUser.userName!)"
        }
        else{
            userNameView.isHidden = true
            userName.text = "-"
        }
        
        if((appUser.countryCode != nil) && (appUser.userPhone != nil)){
            mobileNumber.text = "+\(appUser.userPhone!)"
        }
        else{
            mobileNumber.text = "-"
        }
        }
        else{
            userName.text = "-"
            mobileNumber.text = "-"
        }
        
        
        if(isGreek){
            userNameLabel.text = "Όνομα χρήστη"
            mobileNumberLabel.text = "Τηλέφωνο"
            internationalPreferencesLabel.text = "ΔΙΕΘΝΕΙΣ ΠΡΟΤΙΜΗΣΕΙΣ"
            languageLabel.text = "Γλώσσα"
            language.text = "Ελληνικά"
            navTitle.text = "Προφίλ"
        }
        else{
            userNameLabel.text = "Username"
            mobileNumberLabel.text = "Mobile Number"
            internationalPreferencesLabel.text = "INTERNATIONAL PREFERENCES"
            languageLabel.text = "Language"
            language.text = "English"
            navTitle.text = "Profile"
        }
       
        userNameWidth.constant = userNameLabel.intrinsicContentSize.width
        mobileWidth.constant = mobileNumberLabel.intrinsicContentSize.width
        
    }
    
    func setGreekTabs(){
        if let items = tabBarController?.tabBar.items {
            items[0].title = "Πρόσφατες κλήσεις"
            items[1].title = "Λογαριασμός"
        }
    }
    
    func setEnglishTabs(){
        if let items = tabBarController?.tabBar.items {
            items[0].title = "Recent Calls"
            items[1].title = "Account"
           
        }
    }

    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
