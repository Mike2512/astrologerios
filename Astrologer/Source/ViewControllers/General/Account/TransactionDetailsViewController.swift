//
//  TransactionDetailsViewController.swift
//  Astrologer
//
//  Created by George Rousou on 06/08/2021.
//

import UIKit

class TransactionDetailsViewController: UIViewController {

    @IBOutlet weak var transactionNameLabel: UILabel!
    @IBOutlet weak var transactionName: UILabel!
    @IBOutlet weak var transactionChargeLabel: UILabel!
    @IBOutlet weak var transactionCharge: UILabel!
    @IBOutlet weak var transactionDurationLabel: UILabel!
    @IBOutlet weak var transactionDuration: UILabel!
    @IBOutlet weak var transactionDateLabel: UILabel!
    @IBOutlet weak var transactionDate: UILabel!
    @IBOutlet weak var transactionAmountLabel: UILabel!
    @IBOutlet weak var transactionAmount: UILabel!
    @IBOutlet weak var transactionDetailsView: UIView!
    @IBOutlet weak var transactionDetailsMoreView: UIView!
    var transactionDetail : TransactionModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        transactionDetailsView.layer.cornerRadius = 8
        transactionDetailsView.layer.masksToBounds = true
        transactionDetailsMoreView.layer.cornerRadius = 8
        transactionDetailsMoreView.layer.masksToBounds = true
        if(transactionDetail != nil){
            transactionName.text = transactionDetail.transcactionMadeBy ?? ""
            transactionCharge.text = "€\(transactionDetail.transcactionValue!)"
            transactionDuration.text = "04 : 34"
            transactionDate.text = transactionDetail.transcactionDate ?? ""
            transactionAmount.text = "€\(transactionDetail.transcactionValue!)"
        }
    }
    

    @IBAction func goBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

}
