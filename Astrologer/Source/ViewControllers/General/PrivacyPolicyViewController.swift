//
//  PrivacyPolicyViewController.swift
//  Rich Astrology
//
//  Created by George Rousou on 14/10/2020.
//

import UIKit
import WebKit

class PrivacyPolicyViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        let url = URL(string: "http://richreachcorp.com/wp-content/uploads/RR_App_Privacy_Policy.pdf")!
        webView.load(URLRequest(url: url))
    }
    

    @IBAction func backAction(_ sender: Any) {
        self.navigationController!.popViewController(animated: true)
    }
}
