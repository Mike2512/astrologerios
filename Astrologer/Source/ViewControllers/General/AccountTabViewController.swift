//
//  AccountTabViewController.swift
//  Rich Astrology
//
//  Created by George Rousou on 10/05/2021.
//

import UIKit

class AccountTabViewController: CallSetupViewController {

    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userPhone: UILabel!
 
    @IBOutlet weak var navTitle: UIBarButtonItem!
   
    @IBOutlet weak var myWalletView: UIView!
    @IBOutlet weak var helpView: UIView!
    @IBOutlet weak var settingsView: UIView!
    @IBOutlet weak var historyView: UIView!
    @IBOutlet weak var aboutView: UIView!
    @IBOutlet weak var logOutView: UIView!
    @IBOutlet weak var myWalletLabel: UILabel!
    @IBOutlet weak var historyLabel: UILabel!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var logOutLabel: UILabel!
    @IBOutlet weak var helpLabel: UILabel!
    @IBOutlet weak var settingsLabel: UILabel!
    
    @IBOutlet weak var registeredScreen: UIView!
    @IBOutlet weak var profileView: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle()
      //  viewProfileButton.layer.cornerRadius = 8
      //  loginButton.layer.cornerRadius = 8
        setActions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.tabBarController?.tabBar.isHidden = false
        setView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        

    }
    
    func setView(){
        if(appUser != nil){
            if(!isNullOrEmpty(string: appUser.userName)){
                userName.text = "\(appUser.userName!)"
            }
            else{
                userName.text = "-"
            }
            
            if((appUser.countryCode != nil) && (appUser.userPhone != nil)){
                userPhone.text = "+\(appUser.userPhone!)"
            }
        }
        else{
            userName.text = "-"
            userPhone.text = "-"
        }
        
        if(isGreek){
            myWalletLabel.text = "Τα κέρδη μου"
            historyLabel.text = "Ιστορικό"
            aboutLabel.text = "Σχετικά"
            logOutLabel.text = "Αποσύνδεση"
            navTitle.title = "Λογαριασμός"
            settingsLabel.text = "Ρυθμίσεις"
            helpLabel.text = "Βοήθεια"
        }
        else{
            myWalletLabel.text = "My Earnings"
            historyLabel.text = "History"
            aboutLabel.text = "About"
            logOutLabel.text = "Log Out"
            navTitle.title = "Account"
            settingsLabel.text = "Settings"
            helpLabel.text = "Help"
        }
    }

    func setActions(){
        setWalletClick()
        setAboutClick()
        setHistoryClick()
        setLogoutClick()
        setProfileClick()
    }
    
    func setProfileClick(){
        let profileClick = UITapGestureRecognizer(target: self, action: #selector(self.profileClicked(sender:)))
        profileView.isUserInteractionEnabled = true
        profileView.addGestureRecognizer(profileClick)
    }
    
    func setLogoutClick(){
        let logOutClick = UITapGestureRecognizer(target: self, action: #selector(self.logOutClicked(sender:)))
        logOutView.isUserInteractionEnabled = true
        logOutView.addGestureRecognizer(logOutClick)
    }
    
    func setHistoryClick(){
        let myHistoryClick = UITapGestureRecognizer(target: self, action: #selector(self.historyClicked(sender:)))
        historyView.isUserInteractionEnabled = true
        historyView.addGestureRecognizer(myHistoryClick)
    }
    
    func setAboutClick(){
        let myAboutClick = UITapGestureRecognizer(target: self, action: #selector(self.aboutClicked(sender:)))
        aboutView.isUserInteractionEnabled = true
        aboutView.addGestureRecognizer(myAboutClick)
    }
    
    
    func setWalletClick(){
        let myWalletClick = UITapGestureRecognizer(target: self, action: #selector(self.walletClicked(sender:)))
        myWalletView.isUserInteractionEnabled = true
        myWalletView.addGestureRecognizer(myWalletClick)
    }
    
    @objc func profileClicked(sender:UITapGestureRecognizer) {
        let tabBarStoryboard: UIStoryboard = UIStoryboard(name: "TabBarController", bundle: nil)
        let viewController = tabBarStoryboard.instantiateViewController(withIdentifier: "profileView") as! ProfileViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func logOutClicked(sender:UITapGestureRecognizer) {
        appUser = nil
        astrologyDB.clearContents()
        let LaunchScreenStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let launchScreen =  LaunchScreenStoryBoard.instantiateViewController(withIdentifier: "preLogin") as! PreLoginViewController
        UIApplication.shared.windows.first?.rootViewController = launchScreen
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
    
    @objc func walletClicked(sender:UITapGestureRecognizer) {
        let tabBarStoryboard: UIStoryboard = UIStoryboard(name: "TabBarController", bundle: nil)
        let viewController = tabBarStoryboard.instantiateViewController(withIdentifier: "myEarningView") as! MyEarningViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
 
    
    @objc func historyClicked(sender:UITapGestureRecognizer) {
//        let tabBarStoryboard: UIStoryboard = UIStoryboard(name: "TabBarController", bundle: nil)
//        let viewController = tabBarStoryboard.instantiateViewController(withIdentifier: "historyView") as! NewHistoryViewController
//        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    @objc func aboutClicked(sender:UITapGestureRecognizer) {
        let tabBarStoryboard: UIStoryboard = UIStoryboard(name: "TabBarController", bundle: nil)
        let viewController = tabBarStoryboard.instantiateViewController(withIdentifier: "aboutView") as! NewAboutViewController
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func setNavigationTitle(){
        navTitle.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Poppins-Medium", size: 16.0)!], for: .normal)
    }


}
