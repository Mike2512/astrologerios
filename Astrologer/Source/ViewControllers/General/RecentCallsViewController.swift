//
//  RecentCallsViewController.swift
//  Astrologer
//
//  Created by George Rousou on 03/08/2021.
//

import UIKit
import Sinch
import DropDown

class RecentCallsViewController: CallSetupViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var navTitle: UIBarButtonItem!
    @IBOutlet weak var recentCallsHistory: UITableView!
    @IBOutlet weak var statusImage: UIImageView!
    @IBOutlet weak var statusLabel: UILabel!
    
    let dropDown = DropDown()
    
    var recentCalls : [String] = ["Michalis","Andreas","Nikos","Andria","Panagiwta"]
    var callDetailsDialog : CallDetailsDialog!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationTitle()
        recentCallsHistory.delegate = self
        recentCallsHistory.dataSource = self
        recentCallsHistory.tableFooterView = UIView()
        statusImage.tintColor = UIColor.green
        statusLabel.text = "Online"
        statusLabel.textColor = UIColor.green
        setActions()
    }
    
    func setActions(){
        setStatusClick()
    }
    
    func setStatusClick(){
        let statusClick = UITapGestureRecognizer(target: self, action: #selector(self.statusClicked(sender:)))
        statusView.isUserInteractionEnabled = true
        statusView.addGestureRecognizer(statusClick)
    }
    
    @objc func statusClicked(sender:UITapGestureRecognizer) {
       print("StatusClick")
        dropDown.dataSource = ["Online", "Busy", "Away"]
        dropDown.anchorView = statusView
        dropDown.bottomOffset = CGPoint(x: 0, y: statusView.frame.size.height)
        dropDown.show()
        dropDown.selectionAction = { [self] (index: Int, item: String) in
            if(index == 0){
                statusImage.tintColor = UIColor.green
                statusLabel.text = item
                statusLabel.textColor = UIColor.green
            }
            else if (index == 1){
                statusImage.tintColor = UIColor.orange
                statusLabel.text = item
                statusLabel.textColor = UIColor.orange
            }
            else{
                statusImage.tintColor = UIColor.red
                statusLabel.text = item
                statusLabel.textColor = UIColor.red
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(recentCalls.count > 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "recentCallCell") as! RecentCallTableViewCell
            let item = recentCalls[indexPath.row]
            cell.userName.text = item
            cell.callType.text = "Audio Call"
            cell.dateTime.text = "21/Jul/4 - 14:01"
            cell.selectionStyle = .none
            return cell
        }
       
        return UITableViewCell()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        if(recentCalls.count > 0){
            if let items = tabBarController?.tabBar.items {
                    items.forEach { $0.isEnabled = false }
            }
            let item = recentCalls[indexPath.row]
            let sb = UIStoryboard(name:"CallDetailsDialog",bundle:nil)
            let dialog = sb.instantiateInitialViewController()!
            callDetailsDialog = dialog as? CallDetailsDialog
            self.present(dialog, animated: true)
            callDetailsDialog.setValues(callDetails: item, tabBarHeight: self.tabBarController?.tabBar.frame.height)
            callDetailsDialog.closeRunnable = self.closeDetailsDialog
            
        }
    }
    
    func closeDetailsDialog(){
        if let items = tabBarController?.tabBar.items {
            items.forEach { $0.isEnabled = true }
        }
        callDetailsDialog.dismissDialog()
    }

    func setNavigationTitle(){
        navTitle.setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "Poppins-Medium", size: 16.0)!], for: .normal)
    }
}
