//
//  StartViewController.swift
//  Astrologer
//
//  Created by George Rousou on 02/08/2021.
//

import UIKit

class StartViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let LaunchScreenStoryBoard = UIStoryboard(name: "Main", bundle: nil)
        let launchScreen =  LaunchScreenStoryBoard.instantiateViewController(withIdentifier: "splash") as! SplashScreenViewController
        UIApplication.shared.windows.first?.rootViewController = launchScreen
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    

}
