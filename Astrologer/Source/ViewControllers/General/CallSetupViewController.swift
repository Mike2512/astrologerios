//
//  CallSetupViewController.swift
//  Astrologer
//
//  Created by George Rousou on 05/08/2021.
//

import UIKit
import Sinch

enum EButtonsBar: Int {
    
    case answer
    case decline
    case hangup
    
}

class CallSetupViewController: UIViewController, SINClientDelegate, SINCallDelegate, SINCallClientDelegate {

    func clientDidStart(_ client: SINClient!) {
        print("Client Start")
        print(client.userId)
    }
    
    func clientDidFail(_ client: SINClient!, error: Error!) {
        print("Client Fail")
    }
    
    func callDidEstablish(_ call: SINCall!) {
        //self.view.makeToast("Call establish")
    }
    
    func callDidEnd(_ call: SINCall!) {
        callViewDialog.dismiss(animated: false, completion: nil)
        self.tabBarController?.tabBar.isHidden = false
        if(isGreek){
            self.view.makeToast("Τέλος κλήσης")
        }
        else{
            self.view.makeToast("Call end")
        }
        self.audioController.disableSpeaker()
        self.audioController.stopPlayingSoundFile()
    }
    
    var callViewDialog : CallViewDialog!
    var call: SINCall!
    var audioController:SINAudioController {
           return (sinchClient.audioController())!
    }
    
    func closeDialog(){
        self.tabBarController?.tabBar.isHidden = false
        call.hangup()
    }
    
    func client(_ client: SINCallClient!, willReceiveIncomingCall call: SINCall!) {
        self.call = call
        call.delegate = self
    }
    
    func client(_ client: SINCallClient!, didReceiveIncomingCall call: SINCall!) {
        self.call = call
        call.delegate = self
        self.audioController.enableSpeaker()
        self.audioController.startPlayingSoundFile(pathForSound("incoming.wav"), loop: true)
        self.tabBarController?.tabBar.isHidden = true
        let sb = UIStoryboard(name:"CallViewDialog",bundle:nil)
        let dialog = sb.instantiateInitialViewController()!
        
        callViewDialog = dialog as? CallViewDialog
        self.present(dialog, animated: false)
        callViewDialog.closeRunnable = closeDialog
        callViewDialog.answerRunnable = answerCall
        callViewDialog.speakerRunnable = setSpeaker
        callViewDialog.muteRunnable = setMute
       
    }
    
    func setSpeaker(){
        if(callViewDialog.isSpeakerOn){
            self.audioController.enableSpeaker()
        }
        else{
            self.audioController.disableSpeaker()
        }
    }
    
    func setMute(){
        if(callViewDialog.isMuteOn){
            self.audioController.mute()
        }
        else{
            self.audioController.unmute()
        }
    }
    
    func answerCall(){
        self.audioController.disableSpeaker()
        self.audioController.stopPlayingSoundFile()
        call.answer()
    }
    
    
    func callDidProgress(_ call: SINCall)
    {
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setClient()
        
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSessionRecordPermission.granted:
           setClient()
        case AVAudioSessionRecordPermission.denied:
            print("Pemission denied")
        case AVAudioSessionRecordPermission.undetermined:
            print("Request permission here")
            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                DispatchQueue.main.async { [self] in
                    setClient()
                }
            })
        }
    }
    
    func setClient(){
        sinchClient = Sinch.client(withApplicationKey: "50e2e64e-0edb-4597-858e-a411159f904b", applicationSecret: "StO05OSFGkmpu00551XdwA==", environmentHost: "clientapi.sinch.com", userId: "Astrologer2512")
        //sinchClient = Sinch.client(withApplicationKey: "50e2e64e-0edb-4597-858e-a411159f904b", environmentHost: "clientapi.sinch.com", userId: "RichAstrology", cli: "+442038683654")
        //sinchClient
        sinchClient?.delegate = self
        sinchClient?.setSupportCalling(true)
        sinchClient?.start()
        sinchClient?.startListeningOnActiveConnection()
        sinchClient.call()?.delegate = self
    }
    
}
