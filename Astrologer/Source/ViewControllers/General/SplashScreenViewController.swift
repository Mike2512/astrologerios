//
//  SplashScreenViewController.swift
//  Astrologer
//
//  Created by George Rousou on 02/08/2021.
//

import UIKit

class SplashScreenViewController: UIViewController {

    @IBOutlet weak var bottomLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nsObject = Bundle.main.infoDictionary?["CFBundleShortVersionString"]
        let version = nsObject as! String
        
        bottomLabel.text = "A RichReach App (\(version))"
       
       
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        usleep(2000000)
        astrologyDB.insertStartUpCountryAndLanguage(countryID: selectedCountry.countryID, isGreek: isGreek)
        if(astrologyDB.getLanguage() == "Greek"){
            isGreek = true
        }
        else{
            isGreek = false
        }
        
        transitionToHome()
//
    }
    
    func transitionToHome() {
       let tabBarViewController = UIStoryboard(name: "TabBarController", bundle: nil).instantiateViewController(withIdentifier: "HomeTVC") as! UITabBarController
         view.window?.rootViewController = tabBarViewController
         view.window?.makeKeyAndVisible()
    }

}
