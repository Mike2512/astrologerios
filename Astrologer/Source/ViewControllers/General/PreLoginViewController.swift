//
//  PreLoginViewController.swift
//  Rich Astrology
//
//  Created by George Rousou on 20/05/2021.
//

import UIKit

class PreLoginViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageCollections.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(imageCollections.count > 0){
            let item = imageCollections[indexPath.row]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "prelogin", for: indexPath) as! PreLoginCollectionViewCell
            
            cell.imageWidth.constant = fieldsWidth - 10
            cell.messageWidth.constant = fieldsWidth - 10
            cell.preLoginImage.image = item.preImage
            cell.preLoginMessage.text = item.preName
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    

    @IBOutlet weak var pagingControl: UIPageControl!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var waitingView: UIView!
    @IBOutlet weak var logIn: UIButton!
    @IBOutlet weak var imagesCollections: UICollectionView!
    
    var imageCollections : [PreLoginImageModel] = []
    var fieldsWidth : CGFloat!
    override func viewDidLoad() {
        super.viewDidLoad()
        waitingView.isHidden = true
        fieldsWidth = UIScreen.main.bounds.width * 0.75
        logIn.layer.cornerRadius = 8
        setDelegate()
        setView()
        
    }
    
    func setDelegate(){
        imagesCollections.delegate = self
        imagesCollections.dataSource = self
    }
    
    func setView(){

        
        imageCollections.removeAll()
        
        let entry1 = PreLoginImageModel()
        entry1.preImage = UIImage(named: "PreLogin.png")
       
        entry1.preName = "We are happy to answer your question"
        imageCollections.append(entry1)
        
        imagesCollections.reloadData()
        
        pagingControl.numberOfPages = imageCollections.count
        
    }

   
    func transitionToHome() {

       let tabBarViewController = UIStoryboard(name: "TabBarController", bundle: nil).instantiateViewController(withIdentifier: "HomeTVC") as! UITabBarController
         view.window?.rootViewController = tabBarViewController
         view.window?.makeKeyAndVisible()

   }
    
    
    
    @IBAction func logInButton(_ sender: Any) {
        let verificationStoryBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = verificationStoryBoard.instantiateViewController(withIdentifier: "verification") as! UINavigationController
      
        viewController.modalPresentationStyle = .fullScreen
        UIApplication.shared.windows.first!.rootViewController?.present(viewController, animated: false, completion: nil)
        UIApplication.shared.windows.first?.rootViewController?.modalPresentationStyle = .fullScreen
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
   

}
