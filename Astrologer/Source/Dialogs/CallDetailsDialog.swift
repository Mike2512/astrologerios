//
//  CallDetailsDialog.swift
//  Astrologer
//
//  Created by George Rousou on 06/08/2021.
//

import UIKit

class CallDetailsDialog: UIViewController {

    @IBOutlet weak var closeView: UIView!
    @IBOutlet var mainDialog: UIView!
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var callCharge: UILabel!
    @IBOutlet weak var callDuration: UILabel!
    @IBOutlet weak var callDate: UILabel!
    @IBOutlet weak var callType: UILabel!
    @IBOutlet weak var callTypeImage: UIImageView!
    
    var bottomPadding : CGFloat = 0.0
    var showPaymentViewRunnable : Runnable!
    var closeRunnable : Runnable!
  
    var amountValue : Double!
    var remainingBalance : Double!
    var paymentReferenceStr : String!
    var tabBarHeight : CGFloat! = 0
    
    var availableSeconds : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let onBackgroundClick = UITapGestureRecognizer(target: self, action: #selector(self.onBackgroundClick(sender:)))
        backgroundView.isUserInteractionEnabled = true
        backgroundView.addGestureRecognizer(onBackgroundClick)
        let closeClick = UITapGestureRecognizer(target: self, action: #selector(self.onCloseClick(sender:)))
        closeView.isUserInteractionEnabled = true
        closeView.addGestureRecognizer(closeClick)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            var topPadding = window?.safeAreaInsets.top
            bottomPadding = (window?.safeAreaInsets.bottom)!
        }
        
        self.dialogView.frame.origin.y = UIScreen.main.bounds.size.height
        roundCorners([.topLeft,.topRight], radius: 10, view: dialogView)
        UIView.animate(withDuration: 0.3, animations: { [self] in
            self.mainDialog.backgroundColor = Colors.dialogsBackgroundColor
            
            self.dialogView.frame.origin.y = UIScreen.main.bounds.size.height - self.dialogView.frame.size.height - bottomPadding - (self.tabBarHeight ?? 0)!
        }, completion: nil)
        self.dialogView.isHidden = false
        
    }
    
    @objc func onBackgroundClick(sender:UITapGestureRecognizer) {
        closeRunnable()
    }
    
    @objc func onCloseClick(sender:UITapGestureRecognizer) {
        closeRunnable()
    }
    
    func dismissDialog(){
        self.mainDialog.backgroundColor = UIColor.init(red: 1,
                                                                  green: 1,
                                                                  blue: 1,
                                                                  alpha: 0)
        UIView.animate(withDuration: 0.3, animations: {
           
            self.mainDialog.frame.origin.y = UIScreen.main.bounds.size.height
        }, completion: {(finished: Bool) in
            self.dismiss(animated: true,completion: nil)
        })
    }
    
    func setValues(callDetails: String!, tabBarHeight : CGFloat!){
        userName.text = callDetails ?? ""
        self.tabBarHeight = tabBarHeight
    }

}
