//
//  GoToSettingsDialogViewController.swift
//  Rich Astrology
//
//  Created by George Rousou on 05/11/2020.
//

import UIKit

class GoToSettingsDialogViewController: UIViewController {

    @IBOutlet weak var goToSettingsButton: UIButton!
    @IBOutlet weak var closeView: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet var mainDialog: UIView!
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    
    var goToSettingRunnable : Runnable!
    var closeRunnable : Runnable!
  
    
    var availableSeconds : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let onBackgroundClick = UITapGestureRecognizer(target: self, action: #selector(self.onBackgroundClick(sender:)))
        backgroundView.isUserInteractionEnabled = true
        backgroundView.addGestureRecognizer(onBackgroundClick)
        let closeClick = UITapGestureRecognizer(target: self, action: #selector(self.onCloseClick(sender:)))
        closeView.isUserInteractionEnabled = true
        closeView.addGestureRecognizer(onBackgroundClick)
    
        goToSettingsButton.layer.cornerRadius = 10
        goToSettingsButton.backgroundColor = Colors.rrAstroBlue
        
        if(isGreek){
            titleLabel.text = "Απαιτείται άδεια"
            messageLabel.text = "Για να συνεχίσετε με την εγγραφή σας ενεργοποιήστε τις ειδοποιήσεις της εφαρμογής."
            goToSettingsButton.setTitle("Πηγαίνετε στις Ρυθμίσεις", for: .normal)
        }
        else{
            titleLabel.text = "Permission required"
            messageLabel.text = "To proceed with your registration change Notification permission to ON."
            goToSettingsButton.setTitle("Go to Settings", for: .normal)
    
        }

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        var bottomPadding : CGFloat = 0.0
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            var topPadding = window?.safeAreaInsets.top
            bottomPadding = (window?.safeAreaInsets.bottom)!
        }
        
        self.dialogView.frame.origin.y = UIScreen.main.bounds.size.height
        roundCorners([.topLeft,.topRight], radius: 10, view: dialogView)
        UIView.animate(withDuration: 0.3, animations: {
            self.mainDialog.backgroundColor = Colors.dialogsBackgroundColor
            
            self.dialogView.frame.origin.y = UIScreen.main.bounds.size.height - self.dialogView.frame.size.height - bottomPadding
        }, completion: nil)
        self.dialogView.isHidden = false
        
    }
    
    
    @IBAction func nextAction(_ sender: Any) {
        goToSettingRunnable()
    }
    
    @objc func onBackgroundClick(sender:UITapGestureRecognizer) {
        dismissDialog()
    }
    
    @objc func onCloseClick(sender:UITapGestureRecognizer) {
        dismissDialog()
    }
    
    func dismissDialog(){
        self.mainDialog.backgroundColor = UIColor.init(red: 1,
                                                                  green: 1,
                                                                  blue: 1,
                                                                  alpha: 0)
        UIView.animate(withDuration: 0.3, animations: {
           
            self.mainDialog.frame.origin.y = UIScreen.main.bounds.size.height
        }, completion: {(finished: Bool) in
            self.dismiss(animated: true,completion: nil)
        })
    }
    

}
