//
//  SelectCountryDialogViewController.swift
//  Rich Astrology
//
//  Created by George Rousou on 12/10/2020.
//

import UIKit

class SelectCountryDialogViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate{
    
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet weak var closeDialog: UIButton!
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet var selectCountryView: UIView!
    @IBOutlet weak var dialogHeight: NSLayoutConstraint!
    @IBOutlet weak var countryTable: UITableView!
    @IBOutlet weak var dialogTitle: UILabel!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var countries : [CountryModel] = []
    var filteredCountries : [CountryModel] = []
    var selectedCountry : CountryModel!
    var selectAction : Runnable!
    var isGreek : Bool! = true
    var showCountryCode : Bool! = false
    var shouldShowSearchResults = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setView()
        setActions()
        searchBar.delegate = self
        
    }
    
    func setView(){
        dialogView.isHidden = false
        backGroundView.isHidden = false
        titleView.layer.cornerRadius = 10
        dialogView.layer.cornerRadius = 10
        countryTable.layer.cornerRadius = 10
        self.dialogView.frame.origin.y = (UIScreen.main.bounds.size.height / 2.0) - (self.dialogView.frame.size.height / 2.0)
        
        countryTable.delegate = self
        countryTable.dataSource = self
    }
    
    func setActions(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.dismissD))
        backGroundView.isUserInteractionEnabled = true
        backGroundView.addGestureRecognizer(tap)
    }
    
    @objc
    func dismissD(sender:UITapGestureRecognizer) {
        dismissDialog()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(!shouldShowSearchResults){
            return countries.count
        }
        else{
            return filteredCountries.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if countries.count > 0 {
            var item = CountryModel()
            if(!shouldShowSearchResults){
                 item = countries[indexPath.row]
            }
            else{
                 item = filteredCountries[indexPath.row]
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "countries") as! CountriesTableViewCell
            
            cell.countryFlag.image = item.countryFlag
            cell.countryName.text = "\(item.countryName!) (\(item.countryShortName!))"
            cell.countryCode.text = "+\(item.countryID!)"
            cell.countryNameWidth.constant = cell.countryName.intrinsicContentSize.width
            if(showCountryCode){
                cell.countryCode.isHidden = false
            }
            else{
                cell.countryCode.isHidden = true
            }
            
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(!shouldShowSearchResults){
            selectedCountry = countries[indexPath.row]
        }
        else{
            selectedCountry = filteredCountries[indexPath.row]
        }
        selectAction()
    }
    
    func setDialogView(countries : [CountryModel] , isGreek : Bool, showCountryCode : Bool){
        self.countries = countries
        self.showCountryCode = showCountryCode
        dialogHeight.constant = CGFloat(countries.count * 50) + 50
        if (dialogHeight.constant > (UIScreen.main.bounds.height-300)){
            dialogHeight.constant = UIScreen.main.bounds.height-300
        }
        countryTable.reloadData()
        if (isGreek){
            dialogTitle.text = "Διαλέξτε χώρα"
        }
        else{
            dialogTitle.text = "Select a country"
        }
    }
    
    @IBAction func closeDialogAction(_ sender: Any) {
        dismissDialog()
    }
    
    func dismissDialog(){
        backGroundView.isHidden = true
        dialogView.isHidden = true
        self.dismiss(animated: false,completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
         
           
           if(searchText.count > 0){
               shouldShowSearchResults = true
                filteredCountries.removeAll()
               
               let searchCriteria = searchText + ""
               
               for pr in countries{
                   
                if (pr.countryName.lowercased().contains(searchCriteria.lowercased()))
                   {
                    filteredCountries.append(pr)
                   }
                   
               }
               
            countryTable.reloadData()
           }
           else{
               shouldShowSearchResults = false
               countryTable.reloadData()
           }
           
       }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
      {
          self.searchBar.endEditing(true)
      }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        if !shouldShowSearchResults {
            shouldShowSearchResults = true
            countryTable.reloadData()
        }
    }
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        shouldShowSearchResults = true
        countryTable.reloadData()
    }
    
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        shouldShowSearchResults = false
        countryTable.reloadData()
    }
}
