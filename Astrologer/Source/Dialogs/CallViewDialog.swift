//
//  CallViewDialog.swift
//  Astrologer
//
//  Created by George Rousou on 04/08/2021.
//

import UIKit

class CallViewDialog: UIViewController {

    @IBOutlet var mainDialog: UIView!
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var mainStackView: UIStackView!
    @IBOutlet weak var settingsStackView: UIStackView!
    @IBOutlet weak var closeStackView: UIStackView!
    @IBOutlet weak var speakerView: UIView!
    @IBOutlet weak var muteView: UIView!
    @IBOutlet weak var speakerButton: UIButton!
    @IBOutlet weak var muteButton: UIButton!
    
    var closeRunnable : Runnable!
    var speakerRunnable : Runnable!
    var muteRunnable : Runnable!
    var answerRunnable : Runnable!
    var isSpeakerOn = false
    var isMuteOn = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainStackView.isHidden = false
        settingsStackView.isHidden = true
        closeStackView.isHidden = true
        setActions()
    }
    
    @IBAction func speakerclicked(_ sender: Any) {
        isSpeakerOn = !isSpeakerOn
        if(isSpeakerOn){
            speakerButton.tintColor = UIColor.green
        }
        else{
            speakerButton.tintColor = UIColor.white
        }
        speakerRunnable()
    }
    
    @IBAction func muteclicked(_ sender: Any) {
        isMuteOn = !isMuteOn
        if(isMuteOn){
            muteButton.tintColor = UIColor.green
        }
        else{
            muteButton.tintColor = UIColor.white
        }
        muteRunnable()
    }
    
    func setActions(){
//        let speakerClick = UITapGestureRecognizer(target: self, action: #selector(self.speakerClicked(sender:)))
//        speakerView.isUserInteractionEnabled = true
//        speakerView.addGestureRecognizer(speakerClick)
//        let muteClick = UITapGestureRecognizer(target: self, action: #selector(self.muteClicked(sender:)))
//        muteView.isUserInteractionEnabled = true
//        muteView.addGestureRecognizer(muteClick)
    }
    
    @objc func muteClicked(sender:UITapGestureRecognizer) {
      
    }
    
    @objc func speakerClicked(sender:UITapGestureRecognizer) {
       
    }
    
    @IBAction func answerCallAction(_ sender: Any) {
        mainStackView.isHidden = true
        settingsStackView.isHidden = false
        closeStackView.isHidden = false
        answerRunnable()
    }
    
    @IBAction func cancelCallAction(_ sender: Any) {
        closeRunnable()
        self.dismiss(animated: false, completion: nil)
    }

}
