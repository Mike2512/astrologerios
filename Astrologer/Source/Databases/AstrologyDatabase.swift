//
//  AstrologyDatabase.swift
//  Rich Astrology
//
//  Created by George Rousou on 13/10/2020.
//

import Foundation
import SQLite
import QuartzCore

class AstrologyDatabase : NSObject
{
    let DATABASE_NAME = "AstrologyDatabase"
    
    //tables
    let startUpTable = Table("StartUpTable")
    let userTable = Table("UserTable")
    let callRecordsTable = Table("CallRecordsTable")
    let topUpsTable = Table("TopUpsTable")
    let astrologersFavoriteTable = Table("AstrologersFavoriteTable")
    
    //startUP columns
    let countryID = Expression<Int?>("countryID")
    let language = Expression<String?>("language")
    
    //User columns
    let countryCode = Expression<Int?>("countryCode")
    let phoneNumber = Expression<Int?>("phoneNumber")
    let registrationDateTime = Expression<String?>("registrationDateTime")
    let balance = Expression<Double?>("balance")
    let totalCallsMade = Expression<Int?>("totalCallsMade")
    let totalDurationSecs = Expression<Int?>("totalDurationSecs")
    let stripeID = Expression<String?>("StripeID")
    let userName = Expression<String?>("userName")
    
    //Call records columns
    let callRecordID = Expression<Int64>("callRecordID")
    let astrologerName = Expression<String?>("astrologerName")
    let astrologerID = Expression<Int?>("astrologerID")
    let callDate = Expression<String?>("callDate")
    let callTime = Expression<String?>("callTime")
    let destination = Expression<String?>("destination")
    let callDurationInSecs = Expression<String?>("callDurationInSecs")
    let chargePerMinute = Expression<Double?>("chargePerMinute")
    let VATRate = Expression<Double?>("VATRate")
    let callChargeAmount = Expression<Double?>("callChargeAmount")
    let VATChargeAmount = Expression<Double?>("VATChargeAmount")
    let startBalance = Expression<Double?>("startBalance")
    let endBalance = Expression<Double?>("endBalance")
    let costPerMinute = Expression<Double?>("costPerMinute")
    let costAmount = Expression<Double?>("costAmount")
    let sinchCallID = Expression<String?>("sinchCallID")
    let productOffer = Expression<Double?>("productOffer")
    
    //topUps Columns
    let topUpID = Expression<String?>("topUpID")
    let dateTime = Expression<String?>("dateTime")
    let topUpType = Expression<String?>("topUpType")
    let topUpAmount = Expression<Double?>("topUpAmount")
    let balanceBeforeTopUp = Expression<Double?>("balanceBeforeTopUp")
    let balanceAfterTopUp = Expression<Double?>("balanceAfterTopUp")
    let paymentReferenceNo = Expression<String?>("paymentReferenceNo")
    let reference = Expression<String?>("reference")
    
    let astrologerIDN = Expression<Int>("astrologerIDN")
    let isFavorite = Expression<Bool?>("isFavorite")
    
    var database : Connection!
    var lock = NSObject()
    var version = 0
    
    override init(){
        do{
            let documentDirectory = try FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            let fileUrl = documentDirectory.appendingPathComponent(DATABASE_NAME).appendingPathExtension("sqlite3")
            self.database = try Connection(fileUrl.path)
        } catch {
            print(error)
        }
    }
    
    func checkForUpgrade()
    {
        createTables()
        database.userVersion = version
    }
    
    func createTables()
    {
        objc_sync_enter(lock)
        createStartUpTable()
       // createUserTable()
       // createCallRecordsTable()
       // createTopUpTable()
       // createFavoriteTable()
        objc_sync_exit(lock)
    }
    
    /*******************************************    CREATES       ******************************************/
    
    func createFavoriteTable()
    {
        let createFavoriteTable = self.astrologersFavoriteTable.create { (table) in
            table.column(self.astrologerIDN)
            table.column(self.isFavorite)
            
            table.primaryKey(self.astrologerIDN)
        }
        
        do {
            try self.database.run(createFavoriteTable)
        } catch {
            print(error)
        }
    }
    
    func createStartUpTable()
    {
        let createStartUpTable = self.startUpTable.create { (table) in
            table.column(self.countryID)
            table.column(self.language)
        }
        
        do {
            try self.database.run(createStartUpTable)
        } catch {
            print(error)
        }
    }
    
    func createTopUpTable()
    {
        let createTopUpTable = self.topUpsTable.create { (table) in
            table.column(self.topUpID)
            table.column(self.dateTime)
            table.column(self.phoneNumber)
            table.column(self.topUpType)
            table.column(self.topUpAmount)
            table.column(self.balanceBeforeTopUp)
            table.column(self.balanceAfterTopUp)
            table.column(self.paymentReferenceNo)
            table.column(self.reference)
        }
        
        do {
            try self.database.run(createTopUpTable)
        } catch {
            print(error)
        }
    }
    
    func createUserTable()
    {
        let createUserTable = self.userTable.create { (table) in
            table.column(self.countryCode)
            table.column(self.phoneNumber)
            table.column(self.registrationDateTime)
            table.column(self.balance)
            table.column(self.totalCallsMade)
            table.column(self.totalDurationSecs)
            table.column(self.stripeID)
            table.column(self.userName)
        }
        
        do {
            try self.database.run(createUserTable)
        } catch {
            print(error)
        }
    }
    
    func createCallRecordsTable()
    {
        let createCallRecordsTable = self.callRecordsTable.create { (table) in
            table.column(self.callRecordID, primaryKey : .autoincrement)
            table.column(self.phoneNumber)
            table.column(self.astrologerName)
            table.column(self.astrologerID)
            table.column(self.callDate)
            table.column(self.callTime)
            table.column(self.destination)
            table.column(self.callDurationInSecs)
            table.column(self.chargePerMinute)
            table.column(self.VATRate)
            table.column(self.callChargeAmount)
            table.column(self.VATChargeAmount)
            table.column(self.startBalance)
            table.column(self.endBalance)
            table.column(self.costPerMinute)
            table.column(self.costAmount)
            table.column(self.sinchCallID)
            table.column(self.productOffer)
        }
        
        do {
            try self.database.run(createCallRecordsTable)
        } catch {
            print(error)
        }
    }
    
    /*******************************************    INSERTS       ******************************************/
    
    func insertStartUpCountryAndLanguage(countryID: Int! , isGreek : Bool)
    {
        objc_sync_enter(lock)
        var language : String!
        if(isGreek){
            language = "Greek"
        }
        else{
            language = "English"
        }
        
        do {
            let insertStartUpCountry = self.startUpTable.insert(or: .replace,
                                                                self.countryID <- countryID,
                                                                self.language <- language)
            
            try self.database.run(insertStartUpCountry)
        }
        catch
        {
            print(error)
        }
        objc_sync_exit(lock)
        
    }
    
  
    
    func updateUserName(username : String!)
    {
      
        objc_sync_enter(lock)
        do {
            let updateUserName = self.userTable.update(self.userName <- username)
            try self.database.run(updateUserName)
        }
        catch
        {
            print(error)
        }
        objc_sync_exit(lock)
    }
    

    
    /*******************************************    GETS       ******************************************/
    
    func getFavoriteIDs() -> [Int]!
    {
        var favoriteIDs : [Int] = []
        objc_sync_enter(lock)
        do {

            let data = try self.database.prepare(self.astrologersFavoriteTable.filter(isFavorite == true))
            
            for d in data{
                favoriteIDs.append(d[self.astrologerIDN])
            }
        }
        catch
        {
            print(error)
        }
        
        objc_sync_exit(lock)
        
        return favoriteIDs
        
    }
    
    func getCountryID() ->Int!
    {
        var countryID : Int!
        objc_sync_enter(lock)
        do {
            
            let data = try self.database.prepare(self.startUpTable)
            
            for d in data{
                countryID = d[self.countryID]!
            }
        }
        catch
        {
            print(error)
        }
        
        objc_sync_exit(lock)
        return countryID
    }
    func getLanguage() -> String!
    {
        var language : String!
        objc_sync_enter(lock)
        do {
            
            let data = try self.database.prepare(self.startUpTable)
            
            for d in data{
                language = d[self.language]!
            }
        }
        catch
        {
            print(error)
        }
        
        objc_sync_exit(lock)
        return language
    }
    
    func getUser() -> UserModel!
    {
        var user = UserModel()
        objc_sync_enter(lock)
        do {
            
            let data = try self.database.prepare(self.userTable)
            
            for d in data{
                user.countryCode = d[self.countryCode]!
                user.userPhone = d[self.phoneNumber]!
                user.registrationDateTime = d[self.registrationDateTime]!
                user.balance = d[self.balance]!
                user.totalCallsMade = d[self.totalCallsMade]!
                user.totalDurationSecs = d[self.totalDurationSecs]!
                user.stripeID = d[self.stripeID]!
                user.userName = d[self.userName]
            }
        }
        catch
        {
            print(error)
        }
        
        objc_sync_exit(lock)
        
        if user.userPhone == nil{
            return nil
        }
        else{
            return user
        }
    }
    
    
    /*******************************************    GETS       ******************************************/
    
    func clearContents()
    {
        
    do{
//        try self.database.run(self.callRecordsTable.delete())
//        try self.database.run(self.topUpsTable.delete())
//        try self.database.run(self.userTable.delete())
//        try self.database.run(self.astrologersFavoriteTable.delete())
    } catch {
        print(error)
    }
    
}
    
}
