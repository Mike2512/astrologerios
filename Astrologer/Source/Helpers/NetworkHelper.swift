//
//  NetworkHelper.swift
//  RichReach2
//
//  Created by Eumbrella on 20/02/2018.
//  Copyright © 2018 Eumbrella. All rights reserved.
//

import Foundation
import SystemConfiguration

/// Helper for checking if mobile is connected to the internet.
class NetworkHelper {
    
    static let GOOGLE = "http://www.google.com"
    
    
    /// Check if we mobile can connect to input url.
    ///
    /// - Parameters:
    ///   - urlTemp: The url that we will try to connect to.
    ///   - port: The port.
    /// - Returns: True if mobile reach url, otherwise false.
    static func isReachable(urlTemp : String = NetworkHelper.GOOGLE , port : Int = 80) -> Bool {
        let task = NetworkAsyncTask(url : NetworkHelper.GOOGLE)
        task.doInBackground()
        
        if (task.isInternetAvailable())
        {
            return true
        }
        else
        {
            return false
        }
    }
    
    /// Check if network is available.
    ///
    /// - Returns: True if network is available, otherwise false.
    static func isNetworkAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
            return false
        }
        
        // Working for Cellular and WIFI
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        let ret = (isReachable && !needsConnection)
        
        return ret
    }
}
