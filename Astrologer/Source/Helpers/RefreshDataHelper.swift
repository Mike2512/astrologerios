//
//  RefreshDataHelper.swift
//  Rich Astrology
//
//  Created by George Rousou on 19/05/2021.
//

import Foundation
import UIKit

//func refreshDataGlobal(){
//    appUser = astrologyDB.getUser()
//    setUser()
//    if (appUser != nil){
//        if(appUser.stripeID != nil){
//            userStripeID = appUser.stripeID
//        }
//    }
//}
//
//func setUser(){
//    if(NetworkHelper.isNetworkAvailable()){
//        if (appUser != nil){
//            refreshUser2()
//        }
//        else{
//            setAstrologers()
//        }
//    }
//    else{
////        if(isGreek){
////            self.view.makeToast("Παρακαλώ ελέγξτε την σύνδεση σας στο διαδίκτυο")
////        }
////        else{
////            self.view.makeToast("Please check your Internet Connection.")
////        }
//    }
//}
//
//func refreshUser2(){
//    if(NetworkHelper.isNetworkAvailable()){
//        if (appUser != nil){
//            do
//                {
//                    appUser = try AstrologyWebApi.getUser(phone: String(appUser.userPhone), countryCode: String(appUser.countryCode))
//                }
//            catch{}
//            astrologyDB.insertUser(user: appUser)
//            do
//                {
//                    gfreetopUpsT = []
//                    let freeTopUps = try AstrologyWebApi.getFreeTopUps()
//                    if (freeTopUps != nil){
//                        if let freeTopUps = freeTopUps {
//                            var gCounter = 1
//                            for var ft in freeTopUps{
//                                var freeTopUp = FreeTopUpModel()
//                                freeTopUp.rowID = gCounter
//                                var counter = 1
//                                for var i in ft{
//                                    switch counter {
//                                    case 1:
//                                        freeTopUp.dateTime = i
//                                        break
//                                    case 2:
//                                        freeTopUp.mobileNumber = Int(i)
//                                        break
//                                    case 3:
//                                        freeTopUp.topUpAmount = (i as NSString).doubleValue
//                                        break
//                                    case 4:
//                                        freeTopUp.topUpId = i
//                                        break
//
//                                    default:
//                                        break
//                                    }
//                                    counter += 1
//                                }
//                                if freeTopUp.mobileNumber == appUser.userPhone{
//                                    if(freeTopUp.topUpId == nil || freeTopUp.topUpId == ""){
//                                        freeTopUp.topUpType = TopUpsEnum.Free.rawValue
//                                        gfreetopUpsT.append(freeTopUp)
//                                    }
//
//                                }
//                                gCounter += 1
//                            }
//                        }
//                    }
//
//                    if(gfreetopUpsT.count > 0){
//                        do{
//                            totalFreeAmount = 0
//                            for var f in gfreetopUpsT{
//                                let date = Date()
//                                let df = DateFormatter()
//                                df.dateFormat = "dd/MM/yyyy, HH:mm:ss"
//                                let topUpM = TopUpModel()
//                                topUpM.mobileNumber = appUser.userPhone
//                                topUpM.dateTime = df.string(from: date)
//                                topUpM.id = "Free_\(appUser.userPhone!)_\(topUpM.dateTime!)"
//                                topUpM.balanceBeforeTopUp = appUser.balance
//                                topUpM.balanceAfterTopUp = appUser.balance + f.topUpAmount!
//                                topUpM.topUpAmount = f.topUpAmount!
//                                topUpM.topUpType = TopUpsEnum.Free.rawValue
//                                totalFreeAmount += f.topUpAmount
//                                astrologyDB.insertTopUp(topUp: topUpM)
//
//                                appUser = try AstrologyWebApi.addFreeTopUP(phone: String(appUser.userPhone), topUpID: (topUpM.id ?? ""), amount: topUpM.topUpAmount, dateString: df.string(from: date), topUpType: "Free", rowID: f.rowID!)
//
//                                appUser = try AstrologyWebApi.getUser(phone: String(appUser.userPhone), countryCode: String(appUser.countryCode))
//
//                                astrologyDB.insertUser(user: appUser)
//
//                            }
//                        }
//                        catch{
//                        }
//                    }
//                }
//            catch{}
//            astrologyDB.insertUser(user: appUser)
//            getCallsAndTopUps()
//        }
//    }
//    else{
//        //        if(isGreek){
//        //            self.view.makeToast("Παρακαλώ ελέγξτε την σύνδεση σας στο διαδίκτυο")
//        //        }
//        //        else{
//        //            self.view.makeToast("Please check your Internet Connection.")
//        //        }
//    }
//}
//
//func getCallsAndTopUps(){
//    // indicator.startAnimating()
//    let records = astrologyDB.getCallRecords()
//
//    if(records?.count == 0)
//    {
//
//        do{
//            let calls = try AstrologyWebApi.getCallRecord()
//
//            if (calls != nil){
//                if let calls = calls {
//                    for var c in calls{
//                        var call = CallRecordModel()
//                        var counter = 1
//                        for var i in c{
//                            switch counter {
//                            case 1:
//                                call.mobileNumber = Int(i)
//                                break
//                            case 2:
//                                call.astrologerID = Int(i)
//                                break
//                            case 3:
//                                call.astrologerName = i
//                                break
//                            case 4:
//                                call.callDate = i
//                                break
//                            case 5:
//                                call.callTime = i
//                                break
//                            case 6:
//                                //
//                                break
//                            case 7:
//                                call.destination = i
//                                break
//                            case 8:
//                                call.callDurationInSecs = i
//                                break
//                            case 9:
//                                //
//                                break
//                            case 10:
//                                //
//                                break
//                            case 11:
//                                call.chargePerMinute = (i as NSString).doubleValue
//                                break
//                            case 12:
//                                call.VATRate = (i as NSString).doubleValue
//                                break
//                            case 13:
//                                call.callChargeAmount = (i as NSString).doubleValue
//                                break
//                            case 14:
//                                call.VATChargeAmount = (i as NSString).doubleValue
//                                break
//                            case 15:
//                                call.startBalance = (i as NSString).doubleValue
//                                break
//                            case 16:
//                                call.endBalance = (i as NSString).doubleValue
//                                break
//                            case 17:
//                                call.sinchCallID = i
//                                break
//                            case 18:
//                                //
//                                break
//                            case 19:
//                                //
//                                break
//                            case 20:
//                                call.costPerMinute = (i as NSString).doubleValue
//                                break
//                            case 21:
//                                call.costAmount = (i as NSString).doubleValue
//                                break
//                            default:
//                                break
//                            }
//                            counter += 1
//                        }
//                        if call.mobileNumber == appUser.userPhone{
//                            callRecords.append(call)
//                            astrologyDB.insertCallRecord(call: call)
//                        }
//                    }
//
//                }
//            }
//
//        }
//        catch{ }
//
//        getTopUps()
//
//
//    }
//    else{
//        //self.indicator.stopAnimating()
//        getTopUps()
//    }
//}
//
//
//func getTopUps(){
//    let tops = astrologyDB.getTopUps()
//    if (tops?.count == 0){
//        do{
//            let topUpsL = try AstrologyWebApi.getTopUps()
//
//            if (topUpsL != nil){
//                if let topUpsL = topUpsL {
//                    for var t in topUpsL{
//                        var topUp = TopUpModel()
//                        var counter = 1
//                        for var i in t{
//                            switch counter {
//                            case 1:
//                                topUp.id = i
//                                break
//                            case 2:
//                                topUp.dateTime = i
//                                break
//                            case 3:
//                                topUp.mobileNumber = Int(i)
//                                break
//                            case 4:
//                                if(i == "Paid"){
//                                    topUp.topUpType = TopUpsEnum.Paid.rawValue
//                                }
//                                else{
//                                    topUp.topUpType = TopUpsEnum.Free.rawValue
//                                }
//                                break
//                            case 5:
//                                topUp.topUpAmount = (i as NSString).doubleValue
//                                break
//                            case 6:
//                                topUp.balanceBeforeTopUp = (i as NSString).doubleValue
//                                break
//                            case 7:
//                                topUp.balanceAfterTopUp = (i as NSString).doubleValue
//                                break
//                            default:
//                                break
//                            }
//                            counter += 1
//                        }
//                        if topUp.mobileNumber == appUser.userPhone{
//                            topUps.append(topUp)
//                            astrologyDB.insertTopUp(topUp: topUp)
//                        }
//                    }
//                }
//            }
//
//        }
//        catch{ }
//
//
//        setAstrologers()
//
//
//    }
//    else{
//        //self.indicator.stopAnimating()
//        setAstrologers()
//    }
//}
//
//func setAstrologers(){
//    getAstrologists()
//    getHoroscope()
//    getCategories()
//}
//
//func getCategories(){
//    do{
//    astrologersCategories.removeAll()
//    var categoriesR = try AstrologyWebApi.getCategories(selectedCountry: selectedCountry.countryID)
//    if (categoriesR != nil){
//        if let categoriesR = categoriesR {
//
//            for var a in categoriesR{
//                var ast = AstrologersCategoryModel()
//                var counter = 1
//                for var i in a{
//                    switch counter {
//                    case 1:
//                        ast.id = Int(i)
//                        break
//                    case 2:
//                        ast.categoryName = i
//                        break
//
//                    default:
//                        break
//                    }
//                    counter += 1
//                }
//                astrologersCategories.append(ast)
//            }
//        }
//    }
//
//    astrologersCategories.remove(at: 0)
//
//    let allC = AstrologersCategoryModel()
//    allC.id = 100
//    allC.categoryName = "All"
//    let offersC = AstrologersCategoryModel()
//    offersC.id = 200
//    offersC.categoryName = "Offers"
//
//    astrologersCategories.insert(offersC, at: 0)
//    astrologersCategories.insert(allC, at: 0)
//    }
//    catch{}
//}
//
//func getAstrologists(){
//    do{
//        astrologistsT.removeAll()
//        var astrologists = try AstrologyWebApi.getAstrologists(selectedCountry: selectedCountry.countryID)
//        if (astrologists != nil){
//            if let astrologists = astrologists {
//                for var a in astrologists{
//                    var ast = Astrologist()
//                    var counter = 1
//                    for var i in a{
//                        switch counter {
//                        case 1:
//                            ast.id = Int(i)
//                            break
//                        case 2:
//                            ast.name = i
//                            break
//                        case 3:
//                            ast.image = i
//                            break
//                        case 4:
//                            ast.subject = i
//                            break
//                        case 5:
//                            ast.info = i
//                            break
//                        case 6:
//                            ast.language = i
//                            break
//                        case 7:
//                            ast.destinationSIP = i
//                            break
//                        case 8:
//                            ast.chargePerMinute = (i as NSString).doubleValue
//                            break
//                        case 9:
//                            ast.VATRate = (i as NSString).doubleValue
//                            break
//                        case 10:
//                            ast.costPerMinute = (i as NSString).doubleValue
//                            break
//                        case 11:
//                            ast.isDeleted = i
//                            break
//                        case 12:
//                            ast.destinationNumber = i
//                        case 13:
//                            ast.callCenter = i
//                            break
//                        case 14:
//                            if(!isNullOrEmpty(string: i)){
//                                ast.offer = Double(i)
//                            }
//                            else{
//                                ast.offer = 0.0
//                            }
//                            break
//                        default:
//                            break
//                        }
//                        counter += 1
//                    }
//                    astrologistsT.append(ast)
//                }
//            }
//        }
//        astrologistsT.remove(at: 0)
//
//    }
//    catch{
//
//    }
//}
//
//
//func getHoroscope(){
//    do{
//        horoscopes.removeAll()
//        var horoscopesR = try AstrologyWebApi.getHoroscope(selectedCountry: selectedCountry.countryID)
//        if (horoscopesR != nil){
//            if let horoscopesR = horoscopesR {
//
//                for var a in horoscopesR{
//                    var ast = HoroscopeModel()
//                    var counter = 1
//                    for var i in a{
//                        switch counter {
//                        case 1:
//                            ast.id = Int(i)
//                            break
//                        case 2:
//                            ast.name = i
//                            break
//                        case 3:
//                            ast.greekName = i
//                            break
//                        case 4:
//                            ast.dateRange = i
//                            break
//                        case 5:
//                            ast.description = i
//                            break
//
//                        default:
//                            break
//                        }
//                        counter += 1
//                    }
//                    horoscopes.append(ast)
//                }
//            }
//        }
//        horoscopes.remove(at: 0)
//        setHoroscopeImages()
//
//
//    }
//    catch{
//
//    }
//}
//
//func setHoroscopeImages(){
//    horoscopes[0].image = UIImage(named: "aries.png")
//    horoscopes[1].image = UIImage(named: "taurus.png")
//    horoscopes[2].image = UIImage(named: "gemini.png")
//    horoscopes[3].image = UIImage(named: "cancer.png")
//    horoscopes[4].image = UIImage(named: "leo.png")
//    horoscopes[5].image = UIImage(named: "virgo.png")
//    horoscopes[6].image = UIImage(named: "libra.png")
//    horoscopes[7].image = UIImage(named: "scorpio.png")
//    horoscopes[8].image = UIImage(named: "sagittarius.png")
//    horoscopes[9].image = UIImage(named: "capricorn.png")
//    horoscopes[10].image = UIImage(named: "aquarius.png")
//    horoscopes[11].image = UIImage(named: "pisces.png")
//}
