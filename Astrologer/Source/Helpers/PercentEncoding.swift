//
//  PercentEncoding.swift
//  RichReach2
//
//  Created by Eumbrella on 07/03/2018.
//  Copyright © 2018 Eumbrella. All rights reserved.
//

import Foundation

/// Encodes input string to percent form.
///
/// - Parameter s: The input string
/// - Returns: The encoded string.
func percentEncode(s: String!) -> String
{
    var result = ""
    var encodedChar : String
    
    if(s == nil)
    {
        return ""
    }
    
    for var i in 0..<s.count
    {
        var c : Character! = s[i]
        switch (c)
        {
        case ("Α"):
            encodedChar = "%CE%91"
            break
        case ("Β"):
            encodedChar = "%CE%92"
            break
        case ("Γ"):
            encodedChar = "%CE%93"
            break
        case ("Δ"):
            encodedChar = "%CE%94"
            break
        case ("Ε"):
            encodedChar = "%CE%95"
            break
        case ("Ζ"):
            encodedChar = "%CE%96"
            break
        case ("Η"):
            encodedChar = "%CE%97"
            break
        case ("Θ"):
            encodedChar = "%CE%98"
            break
        case ("Ι"):
            encodedChar = "%CE%99"
            break
        case ("Κ"):
            encodedChar = "%CE%9A"
            break
        case ("Λ"):
            encodedChar = "%CE%9B"
            break
        case ("Μ"):
            encodedChar = "%CE%9C"
            break
        case ("Ν"):
            encodedChar = "%CE%9D"
            break
        case ("Ξ"):
            encodedChar = "%CE%9E"
            break
        case ("Ο"):
            encodedChar = "%CE%9F"
            break
        case ("Π"):
            encodedChar = "%CE%A0"
            break
        case ("Ρ"):
            encodedChar = "%CE%A1"
            break
        case ("Σ"):
            encodedChar = "%CE%A3"
            break
        case ("Τ"):
            encodedChar = "%CE%A4"
            break
        case ("Υ"):
            encodedChar = "%CE%A5"
            break
        case ("Φ"):
            encodedChar = "%CE%A6"
            break
        case ("Χ"):
            encodedChar = "%CE%A7"
            break
        case ("Ψ"):
            encodedChar = "%CE%A8"
            break
        case ("Ω"):
            encodedChar = "%CE%A9"
            break
        case ("α"):
            encodedChar = "%CE%B1"
            break
        case ("β"):
            encodedChar = "%CE%B2"
            break
        case ("γ"):
            encodedChar = "%CE%B3"
            break
        case ("δ"):
            encodedChar = "%CE%B4"
            break
        case ("ε"):
            encodedChar = "%CE%B5"
            break
        case ("ζ"):
            encodedChar = "%CE%B6"
            break
        case ("η"):
            encodedChar = "%CE%B7"
            break
        case ("θ"):
            encodedChar = "%CE%B8"
            break
        case ("ι"):
            encodedChar = "%CE%B9"
            break
        case ("κ"):
            encodedChar = "%CE%BA"
            break
        case ("λ"):
            encodedChar = "%CE%BB"
            break
        case ("μ"):
            encodedChar = "%CE%BC"
            break
        case ("ν"):
            encodedChar = "%CE%BD"
            break
        case ("ξ"):
            encodedChar = "%CE%BE"
            break
        case ("ο"):
            encodedChar = "%CE%BF"
            break
        case ("π"):
            encodedChar = "%CF%80"
            break
        case ("ρ"):
            encodedChar = "%CF%81"
            break
        case ("σ"):
            encodedChar = "%CF%83"
            break
        case ("τ"):
            encodedChar = "%CF%84"
            break
        case ("υ"):
            encodedChar = "%CF%85"
            break
        case ("φ"):
            encodedChar = "%CF%86"
            break
        case ("χ"):
            encodedChar = "%CF%87"
            break
        case ("ψ"):
            encodedChar = "%CF%88"
            break
        case ("ω"):
            encodedChar = "%CF%89"
            break
        case ("ς"):
            encodedChar = "%CF%82"
            break
        case ("ά"):
            encodedChar = "%CE%AC"
            break
        case ("έ"):
            encodedChar = "%CE%AD"
            break
        case ("ό"):
            encodedChar = "%CF%8C"
            break
        case ("ώ"):
            encodedChar = "%CF%8E"
            break
        case ("ή"):
            encodedChar = "%CE%AE"
            break
        case ("ί"):
            encodedChar = "%CE%AF"
            break
        case ("ύ"):
            encodedChar = "%CF%8D"
            break
        case ("ϊ"):
            encodedChar = "%CF%8A"
            break
        case (" "):
            encodedChar = "%20"
            break
        default:
            encodedChar = String(c)
        }
        result += encodedChar
    }
    
    return result
}
