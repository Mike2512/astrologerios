//
//  NetoworkAsyncTask.swift
//  RichReach2
//
//  Created by Eumbrella on 20/02/2018.
//  Copyright © 2018 Eumbrella. All rights reserved.
//

import Foundation
import SystemConfiguration

/// NetworkAsyncTask is responsible for testing the device network connection.
class NetworkAsyncTask {
    var httpResponse = 0
    var url : String
    
    init(url : String) {
        self.url = url
    }
    
    /// Make http request.
    func doInBackground(){
        var request = URLRequest(url: URL(string : url)!)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let postString = ""
        request.httpBody = postString.data(using: .utf8)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
            
            let httpStatus = response as? HTTPURLResponse
            
            if (httpStatus?.statusCode != 200)  // check for http errors
            {
                self.httpResponse = (httpStatus?.statusCode)!
                print("statusCode should be 200, but is \(String(describing: httpStatus?.statusCode))")
                print("response = \(String(describing: response))")
            }
            
            self.httpResponse = (httpStatus?.statusCode)!
            let responseString = String(data: data, encoding: .utf8)
            print("responseString = \(String(describing: responseString))")
        }
        task.resume()
    }
    
    /// Checks if internet is available.
    ///
    /// - Returns: True if internet is available, otherwise false.
    func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if (!SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) )
        {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
}
