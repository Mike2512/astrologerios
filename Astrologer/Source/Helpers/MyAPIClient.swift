//
//  MyAPIClient.swift
//  Rich Astrology
//
//  Created by George Rousou on 21/10/2020.
//

import Stripe
import Alamofire

class MyAPIClient: NSObject, STPCustomerEphemeralKeyProvider {
    enum APIError: Error {
        case unknown
        
        var localizedDescription: String {
            switch self {
            case .unknown:
                return "Unknown error"
            }
        }
    }

    static let sharedClient = MyAPIClient()
    var baseURLString: String? = nil
    var baseURL: URL {
        if let urlString = self.baseURLString, let url = URL(string: urlString) {
            return url
        } else {
            fatalError()
        }
    }
    
    func createCustomerKey(withAPIVersion apiVersion: String, completion: @escaping STPJSONResponseCompletionBlock) {
//        STRIPE_API_VERSION = apiVersion
//        do{
//        stripeParams = try AstrologyWebApi.getEphemeralKey(amount : Int(amountToPayG!*100), apiVersion: STRIPE_API_VERSION, customerID: appUser.stripeID)
//            let jsonEncoder = JSONEncoder()
//            let jsonData = try jsonEncoder.encode(stripeParams.ephemeralKey)
//
//            let json = ((try? JSONSerialization.jsonObject(with: jsonData, options: []) as? [String : Any]) as [String : Any]??)
//            completion(json as! [AnyHashable : Any],nil)
//        }
//        catch{
//            completion(nil, error)
//        }

    }

  
}
