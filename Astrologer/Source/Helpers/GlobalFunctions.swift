//
//  GlobalFunctions.swift
//  Rich Astrology
//
//  Created by George Rousou on 13/10/2020.
//

import Foundation
import UIKit

/// - Parameters:
///   - soundName: The name of sound file.
func pathForSound(_ soundName: String) -> String {
    let resourcePath = Bundle.main.resourcePath! as NSString
    return resourcePath.appendingPathComponent(soundName)
}

/// - Parameters:
///   - corners: The corners we want to round.
///   - radius: The radious of rounded corners.
///   - view: The UIView we want to round the corners.
func roundCorners(_ corners: UIRectCorner, radius: CGFloat, view : UIView) {
    let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
    let mask = CAShapeLayer()
    mask.path = path.cgPath
    view.layer.mask = mask
    view.layer.masksToBounds = true
}


//func setSelectedTap(tab : Tab){
//    selectedTap = tab.tabID
//}
//
//func selectTab(tabN : Int!) -> Tab{
//    var tab = Tab()
//    for t in tabs{
//        if t.tabID == tabN {
//            tab = t
//        }
//    }
//    return tab
//}

func hmsFrom(seconds: Int, completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {

        completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)

}

func getStringFrom(seconds: Int) -> String {

    return seconds < 10 ? "0\(seconds)" : "\(seconds)"
}
func getMinutersStringFrom(minutes: Int) -> String {

    return "\(minutes)"
}

//func showLeftDrawer(controller : UIViewController){
//    let sb = UIStoryboard(name:"Leftdrawer",bundle:nil)
//    let dialog = sb.instantiateInitialViewController()!
//    leftDrawer = dialog as? Leftdrawer
//    controller.present(dialog, animated: true)
//}

func getUpdatedJsonStr(inputString : String) -> String
{
    if (isNullOrEmpty(string: inputString) || inputString.lowercased() == "null")
    {
        return ""
    }
    
    var outputString = inputString.replacingOccurrences(of: "|", with: "\"")
    outputString = outputString.replacingOccurrences(of: "\\\\",with: "\\")
    //outputString = outputString.replacingOccurrences(of: "'",with: "")
    //outputString = outputString.replacingOccurrences(of: "'",with: "")
    //outputString = outputString.replacingOccurrences(of: "\n",with: "")
    //outputString = outputString.replacingOccurrences(of: "\t",with: "")
    //outputString = outputString.replacingOccurrences(of: "{",with: "[")
    //outputString = outputString.replacingOccurrences(of: "}",with: "]")
    if(outputString[0] == "\"")
    {
        outputString = String(outputString.dropFirst(1))
    }
    
    if(outputString[outputString.count - 1] == "\"")
    {
        outputString = String(outputString.dropLast())
    }
    
    return  outputString//System.Text.RegularExpressions.Regex.Unescape(outputString)
}

func isNullOrEmpty (string : String!) -> Bool{
    if(string == nil || string.isEmpty)
    {
        return true
    }
    else
    {
        return false
    }
}
