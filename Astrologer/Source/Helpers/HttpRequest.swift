//
//  HttpRequest.swift
//  RichReach2
//
//  Created by Eumbrella on 16/02/2018.
//  Copyright © 2018 Eumbrella. All rights reserved.
//

import UIKit

/// Helper for making async and non-async http requests.
class HttpRequest {
    
    var requestResponse : String!
    var task : URLSessionDataTask!
    var done = false
    var errorOccurred = false
    var waitingTime = 0.0
    
    /// Make a non-asynchronous http request.
    ///
    /// - Parameters:
    ///   - url: The url of request.
    ///   - params: The params of request.
    /// - Throws: error
    func post(url : String, params : String) throws{
        done = false
        waitingTime = 0
        requestResponse = nil
        let url = URL(string: url)!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.timeoutInterval = 60
        let postString = params
        request.httpBody = postString.data(using: .utf8)
        NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main) { (response, data, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                self.errorOccurred = true
                self.done = true
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, (httpStatus.statusCode != 200 && httpStatus.statusCode != 204) {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                //print("response = \(String(describing: response))")
                self.errorOccurred = true
                self.done = true
            }
            
            let responseString = String(data: data, encoding: .utf8)
            self.requestResponse = responseString
            self.done = true
        }
        

        //self.task.resume()
        repeat {
            RunLoop.current.run(until: Date(timeIntervalSinceNow: 0.2))
            /*sleep(UInt32(1))
             waitingTime += 1
             
             if(waitingTime > 30.0)
             {
             throw Errors.error
             }*/
        } while !self.done && !self.errorOccurred
        
        if(errorOccurred)
        {
            
        }
    }
    
    func get(url : String, params : String) throws{
        done = false
        waitingTime = 0
        requestResponse = nil
        let url = URL(string: url)!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "GET"
        request.timeoutInterval = 60
        let postString = params
        request.httpBody = postString.data(using: .utf8)
        NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main) { (response, data, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                self.errorOccurred = true
                self.done = true
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, (httpStatus.statusCode != 200 && httpStatus.statusCode != 204) {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                //print("response = \(String(describing: response))")
                self.errorOccurred = true
                self.done = true
            }
            
            let responseString = String(data: data, encoding: .utf8)
            self.requestResponse = responseString
            self.done = true
        }
        

        //self.task.resume()
        repeat {
            RunLoop.current.run(until: Date(timeIntervalSinceNow: 0.2))
            /*sleep(UInt32(1))
             waitingTime += 1
             
             if(waitingTime > 30.0)
             {
             throw Errors.error
             }*/
        } while !self.done && !self.errorOccurred
        
        if(errorOccurred)
        {
           
        }
    }
    
    
    
    func post2(url : String, params : String) throws{
        done = false
        waitingTime = 0
        requestResponse = nil
        let url = URL(string: url)!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let postString = params
        request.httpBody = postString.data(using: .utf8)
        NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main) { (response, data, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                self.errorOccurred = true
                self.done = true
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                //print("response = \(String(describing: response))")
                self.errorOccurred = true
                self.done = true
            }
            
            let responseString = String(data: data, encoding: .utf8)
            self.requestResponse = responseString
            self.done = true
        }
        
        //task.resume()
        repeat {
            RunLoop.current.run(until: Date(timeIntervalSinceNow: 0.1))
        } while !done
        
        if(errorOccurred)
        {
           
        }
    }
    
    func post3(url : String, params : String, timeout : Double = 60) throws{
        done = false
        waitingTime = 0
        requestResponse = nil
        let url = URL(string: url)!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        request.timeoutInterval = timeout
        let postString = params
        request.httpBody = postString.data(using: .utf8)
        task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                self.errorOccurred = true
                self.done = true
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, (httpStatus.statusCode != 200 && httpStatus.statusCode != 204) {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                self.errorOccurred = true
                self.done = true
            }
            
            let responseString = String(data: data, encoding: .utf8)
            self.requestResponse = responseString
            self.done = true
        }
        
        task.resume()
        repeat {
            //RunLoop.current.run(until: Date(timeIntervalSinceNow: 0.1))
            sleep(1)
            
            /*sleep(UInt32(1))
             waitingTime += 1
             
             if(waitingTime > 30.0)
             {
             throw Errors.error
             }*/
        } while !done
        
        //throw Errors.error
        
        if(errorOccurred)
        {
           
        }
    }
    
    /// Make an asynchronous http request.
    ///
    /// - Parameters:
    ///   - url: The url of request.
    ///   - params: The params of request.
    /// - Throws: error
    func postAsync(url : String, params : String) throws{
        done = false
        requestResponse = nil
        let url = URL(string: url)!
        var request = URLRequest(url: url)
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpMethod = "POST"
        let postString = params
        request.httpBody = postString.data(using: .utf8)
        NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.main) { (response, data, error) in
            guard let data = data, error == nil else {                                                 // check for fundamental networking error
                print("error=\(String(describing: error))")
                self.errorOccurred = true
                return
            }
            
            if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(String(describing: response))")
                self.errorOccurred = true
            }
            
            let responseString = String(data: data, encoding: .utf8)
            self.requestResponse = responseString
            self.done = true
        }
        
        //task.resume()
    }
}
