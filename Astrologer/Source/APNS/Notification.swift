//
//  Notification.swift
//  RichReach2
//
//  Created by Eumbrella on 27/03/2018.
//  Copyright © 2018 Eumbrella. All rights reserved.
//

import Foundation


/// Notification holds all the info associated to a push notification that is sent through APNS.
/// Each notification has a campaign ID, the name of the business that created the campaign and the business image URI and a message to be displayed.
class Notification
{
    var functionID : Int!
    var notificationID : Int!
    var type : String!
    var dataID : Int!
    var campaignID : Int64!
    var orderID : Int64!
    var partnerImageURI : String!
    var header : String!
    var body : String!
    var apk : String!
    var date : String!
    var dateD : Date!
    var clientID : Int64!
    var projectName : String!
    var channelId : String!
    var channelName : String!
    var imageUrl : String!
}
