//
//  APNSManager.swift
//  RichReach2
//
//  Created by Eumbrella on 27/03/2018.
//  Copyright © 2018 Eumbrella. All rights reserved.
//

import Foundation
import UIKit
import Firebase

/// APNSManager provides the basic functionality that allows us to perform actions related to APNS (Apple Push Notification Services).
class APNSManager
{
    var yesAction : Runnable!
    var noAction : Runnable!
    var token : String!
    var notification = Notification()
    var Window : UIWindow!
 
    var ok = false


    /// Processes the push notification by:
    /// a) Extracting the notification information into a push notification object.
    /// b) Notifying the server about the notification delivery.
    /// c) Inserting the notification to the device's local database.
    ///
    /// - Parameters:
    ///   - notificationInformation: The notification information dictionary.
    ///   - displayAlertBool: A flag that indicates whether an alert should be displayed to the user or not.
    ///   - window: The application window.
    func processNotification(notificationInformation : [AnyHashable : Any], displayAlertBool : Bool, window : inout UIWindow)
    {
        //UIApplication.shared.applicationIconBadgeNumber += 1

        let notification = getNotification(notificationInformation: notificationInformation)

       // notificationDB.insertNotification(notification: notification, selectedOptions: selectedOptions)


        if(displayAlertBool)
        {
            displayAlert(notification : notification, window : &window)
        }
        else
        {
           
            transitionToHome(window : &window)

        }
    }
    

    func transitionToHome(window : inout UIWindow) {
        openFromNotification = true
        let tabBarViewController = UIStoryboard(name: "TabBarController", bundle: nil).instantiateViewController(withIdentifier: "HomeTVC") as! UITabBarController
        tabBarViewController.selectedIndex = 1
        
        if #available(iOS 13.0, tvOS 10.0, macOS 10.12, *)
        {
            window.makeKeyAndVisible()
          
            tabBarViewController.modalPresentationStyle = .fullScreen
            window.rootViewController = tabBarViewController
            window.makeKeyAndVisible()
        }else
        {
            window.makeKeyAndVisible()
            tabBarViewController.modalPresentationStyle = .fullScreen
            window.rootViewController = tabBarViewController
            //window.makeKeyAndVisible()
        }
   }
    func getNotification(notificationInformation : [AnyHashable : Any]) -> Notification
    {
        let aps = notificationInformation[AnyHashable("aps")] as! NSObject

        let alert = aps.value(forKey: "alert") as! NSDictionary
        let notificationTypeObj = notificationInformation[AnyHashable("FunctionID")] as? String
        if (notificationTypeObj != nil){
            notification.functionID = Int(notificationTypeObj!)
        }
        notification.imageUrl = notificationInformation[AnyHashable("ImageUrl")] as? String
        notification.type = notificationInformation[AnyHashable("type")] as? String
        notification.header = alert.value(forKey: "title") as? String //alert![AnyHashable("title")] as! String
        notification.body = alert.value(forKey: "body") as? String //alert.value(forKey: "body") as! String

        return notification
    }

    /// Displays a notification alert to the user using our custom bottom drawer.
    ///
    /// - Parameters:
    ///   - notification: The notification.
    ///   - window: The application window.
    func displayAlert(notification : Notification, window : inout UIWindow)
    {
        do
        {
            

        }
        catch
        {

        }
    }


    /// Returns the current vissible view controller.
    ///
    /// - Parameter window: The application window.
    /// - Returns: he current vissible view controller.
    func getCurrentViewController(window : UIWindow) -> UIViewController
    {
        var currentViewController = window.rootViewController

        while (currentViewController?.presentedViewController != nil)
        {
            currentViewController = currentViewController?.presentedViewController
        }

        return currentViewController!
    }

    /// This function is been called when user select yes at bottom drawer.
    func yesRaised() {
        yesAction()
    }

    /// This function is been called when user select no at bottom drawer.
    func noRaised() {
        noAction()
    }

}
