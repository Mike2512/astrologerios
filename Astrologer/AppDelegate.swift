//
//  AppDelegate.swift
//  Astrologer
//
//  Created by George Rousou on 02/08/2021.
//

import UIKit
import SystemConfiguration
import UserNotifications
import Reachability
import CoreLocation
import IQKeyboardManagerSwift
import Firebase
import Sinch
import Stripe
import FirebaseMessaging
import FirebaseDynamicLinks
import FirebaseCrashlytics
import FirebaseRemoteConfig

var sinchClient : SINClient!
var astrologyDB: AstrologyDatabase!
var countries : [CountryModel] = []
var isGreek = true
var appVersion = ""
var appType = ""
var appDelegate : AppDelegate!
var selectedCountry : CountryModel!
var hasActivityBeenCreatedFromNotificationsView : Bool!
var userIsSignedIn : Bool! = false
var openFromNotification : Bool = false
var appUser: UserModel!

typealias Runnable = () -> ()

@main
class AppDelegate: UIResponder, UNUserNotificationCenterDelegate , UIApplicationDelegate, SINClientDelegate {
    

    let gcmMessageIDKey = "gcm.message_id"
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.isIdleTimerDisabled = true
        
        appVersion = Bundle.mainAppVersion!
        appType = "iOS"
        appDelegate = self
        IQKeyboardManager.shared.enable = true
        setCountries()
        astrologyDB = AstrologyDatabase()
        astrologyDB.checkForUpgrade()
        
        FirebaseApp.configure()
        
        signOutOldUser()
        
        registerForPushNotifications()
       
        checkIfUserIsLoggedIn()
        
        initSinchClient()
        
        return true
    }
    
    func initSinchClient() {
        sinchClient = Sinch.client(withApplicationKey: "50e2e64e-0edb-4597-858e-a411159f904b", applicationSecret: "StO05OSFGkmpu00551XdwA==", environmentHost: "clientapi.sinch.com", userId: "Astrologer2512")
        //sinchClient = Sinch.client(withApplicationKey: "50e2e64e-0edb-4597-858e-a411159f904b", environmentHost: "clientapi.sinch.com", userId: "RichAstrology", cli: "+442038683654")
        //sinchClient
        sinchClient?.delegate = self
        sinchClient?.setSupportCalling(true)
        sinchClient?.start()
        sinchClient?.startListeningOnActiveConnection()
    }
    
    func checkIfUserIsLoggedIn(){
        Auth.auth().addStateDidChangeListener { auth, user in
            if let user = user {
                userIsSignedIn = true
            } else {
                userIsSignedIn = false
            }
        }
    }
    
    func registerForPushNotifications() {
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM)
            //Messaging.messaging().delegate = self
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        UIApplication.shared.registerForRemoteNotifications()
        
    }
   
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//
//    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
      withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
      let userInfo = notification.request.content.userInfo


      // Print full message.
      print(userInfo)

      // Change this to your preferred presentation option
       completionHandler([.alert,.sound, .badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                 didReceive response: UNNotificationResponse,
                                 withCompletionHandler completionHandler: @escaping () -> Void) {
       let userInfo = response.notification.request.content.userInfo

       // Print full message.
       print(userInfo)
     callApnsManager(userInfo: userInfo, applicationState: UIApplication.shared.applicationState)
     }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print(userInfo)
        
    }
    
    func callApnsManager(userInfo : [AnyHashable : Any], applicationState : UIApplication.State){
        //let application = UIApplication.shared
        let apnsManager = APNSManager()
                
        if ((applicationState == UIApplication.State.background))
        {
            hasActivityBeenCreatedFromNotificationsView = true
            apnsManager.processNotification(notificationInformation: userInfo, displayAlertBool: false, window: &self.window!)
        }
        else if ((applicationState == UIApplication.State.inactive))
        {
            hasActivityBeenCreatedFromNotificationsView = false
            apnsManager.processNotification(notificationInformation: userInfo, displayAlertBool: false, window: &self.window!)
        }
        else
        {
            hasActivityBeenCreatedFromNotificationsView = false
            apnsManager.processNotification(notificationInformation: userInfo, displayAlertBool: true, window: &self.window!)
        }
        
    }

    func setCountries(){
        let country1 = CountryModel()
        country1.countryID = 357
        country1.countryName = "Cyprus"
        country1.countryShortName = "CY"
        country1.countryFlag = UIImage(named: "Cyprus.png")
        countries.append(country1)
        let country2 = CountryModel()
        country2.countryID = 30
        country2.countryName = "Greece"
        country2.countryShortName = "GR"
        country2.countryFlag = UIImage(named: "Greece.png")
        countries.append(country2)
        let country3 = CountryModel()
        country3.countryID = 44
        country3.countryName = "United Kingdom"
        country3.countryShortName = "GB"
        country3.countryFlag = UIImage(named: "UnitedKingdom.png")
        countries.append(country3)
        let country4 = CountryModel()
        country4.countryID = 49
        country4.countryName = "Germany"
        country4.countryShortName = "De"
        country4.countryFlag = UIImage(named: "Germany.png")
        countries.append(country4)
        let country5 = CountryModel()
        country5.countryID = 31
        country5.countryName = "Netherlands"
        country5.countryShortName = "NL"
        country5.countryFlag = UIImage(named: "Netherlands.png")
        countries.append(country5)
        let country6 = CountryModel()
        country6.countryID = 32
        country6.countryName = "Belgium"
        country6.countryShortName = "BE"
        country6.countryFlag = UIImage(named: "Belgium.png")
        countries.append(country6)
        let country7 = CountryModel()
        country7.countryID = 46
        country7.countryName = "Sweden"
        country7.countryShortName = "SE"
        country7.countryFlag = UIImage(named: "Sweden.png")
        countries.append(country7)
        let country8 = CountryModel()
        country8.countryID = 41
        country8.countryName = "Switzerland"
        country8.countryShortName = "CH"
        country8.countryFlag = UIImage(named: "Switzerland.png")
        countries.append(country8)
        let country9 = CountryModel()
        country9.countryID = 33
        country9.countryName = "France"
        country9.countryShortName = "FR"
        country9.countryFlag = UIImage(named: "France.png")
        countries.append(country9)
        let country10 = CountryModel()
        country10.countryID = 43
        country10.countryName = "Austria"
        country10.countryShortName = "AT"
        country10.countryFlag = UIImage(named: "Austria.png")
        countries.append(country10)
        let country11 = CountryModel()
        country11.countryID = 34
        country11.countryName = "Spain"
        country11.countryShortName = "ES"
        country11.countryFlag = UIImage(named: "Spain.png")
        countries.append(country11)
        let country12 = CountryModel()
        country12.countryID = 1
        country12.countryName = "United States"
        country12.countryShortName = "US"
        country12.countryFlag = UIImage(named: "UnitedStates.png")
        countries.append(country12)
        let country13 = CountryModel()
        country13.countryID = 1
        country13.countryName = "Canada"
        country13.countryShortName = "CA"
        country13.countryFlag = UIImage(named: "Canada.png")
        countries.append(country13)
        let country14 = CountryModel()
        country14.countryID = 61
        country14.countryName = "Australia"
        country14.countryShortName = "AU"
        country14.countryFlag = UIImage(named: "Australia.png")
        countries.append(country14)
        let country15 = CountryModel()
        country15.countryID = 380
        country15.countryName = "Ukraine"
        country15.countryShortName = "UA"
        country15.countryFlag = UIImage(named: "Ukraine.png")
        countries.append(country15)
        let country16 = CountryModel()
        country16.countryID = 7
        country16.countryName = "Russia"
        country16.countryShortName = "RU"
        country16.countryFlag = UIImage(named: "Russia.png")
        countries.append(country16)
        selectedCountry = country1
    }
    
    func clientDidStart(_ client: SINClient!) {
        print(client.userId ?? "")
        print("started")
    }
    
    func clientDidFail(_ client: SINClient!, error: Error!) {
        print("fail")
    }

}

extension AppDelegate{
func signOutOldUser(){
    if let _ = UserDefaults.standard.value(forKey: "isNewuser"){}else{
        do{
            UserDefaults.standard.set(true, forKey: "isNewuser")
            try Auth.auth().signOut()
        }catch{}
    }
}
}
