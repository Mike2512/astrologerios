//
//  Colors.swift
//  Rich Astrology
//
//  Created by George Rousou on 16/10/2020.
//

import UIKit

struct Colors {
    static let rrAstroBlack = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 1)
    static let rrAstroRed = UIColor(red: 255/255, green: 3/255, blue: 3/255, alpha: 1)
    static let rrAstroBlue = UIColor(red: 70/255, green: 100/255, blue: 209/255, alpha: 1)
    static let rrAstroBlueA = UIColor(red: 71/255, green: 90/255, blue: 167/255, alpha: 0.5)
    static let rrAstroPurple = UIColor(red: 120/255, green: 135/255, blue: 217/255, alpha: 1)
    static let rrAstroPurpleA = UIColor(red: 120/255, green: 135/255, blue: 217/255, alpha: 0.5)
    static let rrAstroLila = UIColor(red: 114/255, green: 29/255, blue: 114/255, alpha: 1)
    static let rrAstroBlueNew = UIColor(red: 70/255, green: 100/255, blue: 209/255, alpha: 1)
    static let rrAstroGrayS = UIColor(red: 131/255, green: 131/255, blue: 131/255, alpha: 1)
    static let rrAstroGrayBackground = UIColor(red: 247/255, green: 247/255, blue: 247/255, alpha: 1)
    static let dialogsBackgroundColor = UIColor(red: 50.0/255, green:50.0/255, blue:50.0/255, alpha:0.8)
}

